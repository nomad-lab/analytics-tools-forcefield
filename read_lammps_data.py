import json

class TimeStep(object):
    def __init__(self):
        self.confs = []
        self.force = []
        self.box_type = []
        self.box_size = []
        self.xlo, self.xhi = 0., 0.
        self.ylo, self.yhi = 0., 0.
        self.zlo, self.zhi = 0., 0.

    def convert(self):

        from ase.io import extxyz
        import numpy as np
        from ase.neighborlist import NeighborList
        import ase

        atoms = ase.atoms.Atoms(
            'Si'*len(self.confs),
            positions=np.asarray(self.confs, dtype=np.float),
            cell=[self.xhi-self.xlo, self.yhi-self.xlo,self.zhi-self.zlo],
            pbc=[1, 1, 1],
        )


        N = len(self.confs)

        r_cut = 5  # 3.5  4.4

        cutoffs = np.ones(len(atoms)) * r_cut / 2.
        nl = NeighborList(cutoffs, skin=0., sorted=False, self_interaction=False,
                          bothways=True)
        nl.build(atoms)


        confs=[]

        cell = atoms.get_cell()

        indices, offsets = nl.get_neighbors(0)
        offsets = np.dot(offsets, cell)
        conf = np.zeros((len(indices), 3))

        for i, (a2, offset) in enumerate(zip(indices, offsets)):
            d = atoms.positions[a2] + offset - atoms.positions[0]
            conf[i] = d

        return conf, self.force[0]





        #
        # cutoffs = np.ones(N) * r_cut / 2.
        # print(cutoffs)
        # nl = NeighborList(cutoffs, skin=0., sorted=False, self_interaction=False, bothways=True)
        #
        # confs = []
        # forces = np.zeros((np.round(steps / every), 1, 3))
        #
        # for n in np.arange(np.round(steps / every)):
        #     atoms = extxyz.read_extxyz('disloiron.xyz', index=n * every)
        #     forces[n] = atoms.get_array('force')[0]
        #
        #     cutoffs = np.ones(len(atoms)) * r_cut / 2.
        #     nl = NeighborList(cutoffs, skin=0., sorted=False, self_interaction=False,
        #                       bothways=True)
        #     nl.build(atoms)
        #
        #     cell = atoms.get_cell()
        #
        #     indices, offsets = nl.get_neighbors(0)
        #     offsets = np.dot(offsets, cell)
        #     conf = np.zeros((len(indices), 3))
        #
        #     for i, (a2, offset) in enumerate(zip(indices, offsets)):
        #         d = atoms.positions[a2] + offset - atoms.positions[0]
        #         conf[i] = d
        #     confs.append(conf)
        #
        # np.save("confst.npy", confs)
        # np.save("forcest.npy", forces)
        #
        # lens = []
        # for i in np.arange(len(confs)):
        #     lens.append(len(confs[i]))
        #
        # print(max(lens))
        # print(min(lens))
        # print(np.mean(lens))


class ReadLammpsDumpFile():
    def __init__(self, filename: str):
        self.filename = filename
        self.counter = 0  # type: int

    def __enter__(self):
        # print('enter')
        self.fid = open(self.filename, 'r')
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        # print('exit')
        self.fid.close()
        pass

    def __iter__(self):
        # print("iter")
        return self

    def __next__(self):
        try:
            timestep = self.readtimestep()

            # print('next')

        except:
            raise StopIteration

        # if self.counter > 1:
        #     raise StopIteration
        # else:
        #     self.counter += 1

        return timestep

    def readtable(self, natoms):

        item = self.fid.readline()
        words = item.split()[2:]

        # print(words)

        confs = []
        force = []
        for i in range(natoms):
            words = self.fid.readline().split()
            confs.append(words[2:5])
            force.append(words[5:8])
            # print(words)

        return confs, force


    def readtimestep(self):

        timestep = TimeStep()

        self.fid.readline() # ITEM: TIMESTEP
        timestep.time = int(self.fid.readline().split()[0])  # just grab 1st field

        print(timestep.time)

        self.fid.readline() # ITEM: NUMBER OF ATOMS
        timestep.natoms = int(self.fid.readline())

        item = self.fid.readline()  # ITEM: BOX BOUNDS .. .. ..
        words = item.split("BOUNDS ")

        words = self.fid.readline().split()
        timestep.xlo, timestep.xhi = float(words[0]), float(words[1])

        words = self.fid.readline().split()
        timestep.ylo, timestep.yhi = float(words[0]), float(words[1])

        words = self.fid.readline().split()
        timestep.zlo, timestep.zhi = float(words[0]), float(words[1])

        timestep.confs, timestep.force = self.readtable(timestep.natoms)


        return timestep

class JsonWriter():
    def __init__(self, filename):
        pass


def write_json(output, confs, forces):

    # print(json.dumps({'confs': confs[0].tolist()}))
    # print(json.dumps({'force': forces}))
    # print(json.dumps({'confs': confs[0].tolist(), 'force': forces[0].tolist()}))

    out = []
    for c, f in zip(confs, forces):
        out.append({'confs': c.tolist(), 'force': f})

    with open(output, 'w') as outfile:
        json.dump({'data': out}, outfile)



if __name__ == '__main__':

    filename='data-lammps/Silic_2000/Si_2000_dump.atom'
    output = 'Si_2000K.json'


    confs = []
    forces = []

    with ReadLammpsDumpFile(filename) as file:

        for timestep in file:
            conf, force = timestep.convert()

            confs.append(conf)
            forces.append(force)

            # print(timestep)
            # print(timestep.confs)
            # print(timestep.force)


    write_json(output, confs, forces)




