# ML of Quantum Forces

## Description

Gaussian Process regression module to learn quantum forces on local configurations.


## Code Example


First import the module

```
import sys
sys.path.insert(0, 'ML_of_Forces/2D')

from  GP_custom_gen import GaussianProcess3 as GPvec
```

Then import your training database

```
tr_confs = np.genfromtxt("configurations.txt")
tr_forces = np.genfromtxt("forces.txt")
```

Initialise the GP process
```
gp = GPvec( ker=['sim'], fvecs =['cart'], sig = 0.5)
```
Fit the training data

```
gp.fit(tr_confs, tr_forces)
```

Predict forces on new configurations

```
new_forces = gp.predict(new_confs)
```

## Installation

Just download the package via

```
git clone git@gitlab.com:Glielmo/ML_of_Forces.git
```

You will need
- numpy
- scipy

and, optionally
- sklearn

## Author

Aldo Glielmo, King's College London

<aldo.glielmo@kcl.ac.uk>



