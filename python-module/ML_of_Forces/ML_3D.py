import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA
import random
from mpl_toolkits.mplot3d import axes3d
from GP_custom_gen import GaussianProcess3 as GPvec
#from GP_custom_gen_MulChem import GaussianProcess3 as GPvec
import os.path
import datetime


### Importing data from simulation ###

InDir = "../ML_Data/TB/a-Si"

forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))

confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))

print("Force and confs shapes are ", np.shape(forces), np.shape(confs))
print("Typical configuration length is ", len(confs[0]))
lenc = len(forces)

print("Database lenght is: ", lenc)


### Subsampling from big database ###

ncal = 100
ntest = 100

ntot = ncal + ntest

ind = np.arange(lenc)

ind_ntot = np.random.choice(ind, size=ntot, replace=False)
ind_ncal = ind_ntot[0:ncal]
ind_ntest = ind_ntot[ncal:ntot]


print("Ntot Database lenght is: " ,len(ind_ntot))

### Spliting database in training/testing sets ###

tr_confs = confs[ind_ncal]
tr_forces = forces[ind_ncal]

tst_confs =  confs[ind_ntest]
tst_forces = forces[ind_ntest]

#tst_confs = tr_confs
#tst_forces = tr_forces

##########################


print("Initial shapes are ",np.shape(tr_confs[0]), np.shape(tr_forces), np.shape(tst_confs), np.shape(tst_forces))
print("One conf one force ", confs[0][ 0, :], forces[0])
print("Training Database lenght is: " ,len(tr_confs))
print("Testing Database lenght is: " ,len(tst_confs))

### Training and testing of the GP model ###

t0 = datetime.datetime.now()

### Training the GP model ###

# optimization methods: None, "fmin_l_bfgs_b", "Nelder-Mead"

t0train = datetime.datetime.now()
#nugget = 1e-20
# nugget = 1e-8
gp = GPvec( ker=[ 'id'], fvecs =['cov_sim'] ,nugget = 1e-14, theta0=np.array([1.]), sig =1., bounds = ((0.1,10.),),
                        optimizer=  None, calc_error = False, eval_grad = False)

gp.fit(tr_confs, tr_forces)

tftrain = datetime.datetime.now()
print("Training computational time is", (tftrain-t0train).total_seconds())

### Testing the GP model ###

t0test = datetime.datetime.now()
f_pred = gp.predict(tst_confs)                   # , err_pred 
print("f_pred", np.shape(f_pred))
f_pred = np.reshape(f_pred, (ntest, 3)) #f_pred.resize(ntest, 3)
tftest = datetime.datetime.now()
print("Testing computational time is", (tftest-t0test).total_seconds())

tf = datetime.datetime.now()
print("Computational time is", (tf-t0).total_seconds())

### Testing performance of the GP Model ###
tst_forces = np.reshape(tst_forces, (ntest, 3))#tst_forces.resize(ntest, 3)
SSres = np.sum((f_pred-tst_forces)**2)
SStot = np.sum((tst_forces-np.mean(tst_forces))**2)
R2 = 1 - SSres/SStot
print("Rsquared value is", R2)

errors = np.zeros(len(tst_forces))
errors_c = f_pred-tst_forces
errors_c = np.abs(errors_c)

for i in np.arange(len(errors)):
    errors[i] = np.linalg.norm(f_pred[i]-tst_forces[i])

mean_f = np.mean(np.sqrt(np.sum(tst_forces**2, axis = 1)))
print(mean_f)
print("Mean value of error is ", np.mean(errors), "Mean force in testing set is ", mean_f, "Mean relative error is ", np.mean(errors)/mean_f)

### Plots ###
fs = 15

fig = plt.figure(1)
#plt.plot(tr_forces, tr_forces, 'go')
plt.plot(tst_forces[:,0], tst_forces[:,0], 'b-')
plt.plot( f_pred[:,0],tst_forces[:,0], 'ro')
plt.plot(tst_forces[:,1], tst_forces[:,1], 'b-')
plt.plot( f_pred[:,1],tst_forces[:,1], 'ro')
plt.plot(tst_forces[:,2], tst_forces[:,2], 'b-')
plt.plot( f_pred[:,2],tst_forces[:,2], 'ro')
plt.ylabel('real value ' + r'$(\rm{eV/ \AA})$', fontsize = fs)
plt.xlabel('prediction ' + r'$(\rm{eV/ \AA})$', fontsize = fs)
plt.show()


fig = plt.figure(2)
print(np.shape(errors_c), np.shape(err_pred))
#err_pred = err_pred.reshape(ntest*3)
print(np.shape(errors_c))
#err_pred = err_pred.sum(1)
plt.plot(errors, errors)
plt.plot( 2*np.sqrt(err_pred),errors_c, 'ro')
plt.ylabel('real value ' + r'$(\rm{eV/ \AA})$', fontsize = fs)
plt.xlabel('prediction ' + r'$(\rm{eV/ \AA})$', fontsize = fs)
plt.show()


fig = plt.figure(3)

#plt.hist(errors, bins = 40, normed = 1., alpha = 0.75, range=(0, 4))
plt.hist(np.sqrt(err_pred.flatten()), bins = 15, normed = 1., alpha = 0.75)
plt.ylabel('$p(\Delta F)$', fontsize = fs)
plt.xlabel('Error on prediction $(\Delta F)$', fontsize = fs)
plt.show()

