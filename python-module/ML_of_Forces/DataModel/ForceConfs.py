from .baseclasses import DataModel
from collections import namedtuple
import numpy as np


### Subsampling from big database ###

### Spliting database in training/testing sets ###


class ForceConfs(DataModel):

    def __init__(self, confs=[], force=[]):
        """

        :param force: array of forces
        :type force: np.array
        :param confs: array of configurations
        """
        assert len(confs) == len(force)

        self.confs = confs
        self.force = force

    def __str__(self):

        out = 'ForceConfs model\n' + \
              '       # of configuration: {}\n'.format(len(self.confs)) +\
              '  range of configurations: mean={2}, min={0}, max={1}\n'.format(*self.range())

        return out

    def __len__(self):
        """

        :return: length of the forces/configurations
        """
        return len(self.confs)

    def range(self):

        """
        Determine the minimum and maximum length for a given list of configurations
        :return: namedtuple('LengthRange', ['min', 'max', 'mean'])
        """
        lengths = [len(conf) for conf in self.confs]

        LengthRange = namedtuple('LengthRange', ['min', 'max', 'mean'])
        return LengthRange(min=np.min(lengths), max=np.max(lengths), mean=np.mean(lengths))

        # return np.min(lengths), np.max(lengths), np.mean(lengths)

    def read_json_data(self, filename):
        import json

        with open(filename, 'r') as jsonfile:
            data = json.load(jsonfile)['data']

        confs = [np.array(timestep['confs'], dtype=np.float) for timestep in data]
        force = [np.array(timestep['force'], dtype=np.float) for timestep in data]

        self.confs = np.asarray(confs)
        self.force = np.asarray(force)


    def subsampling(self, train: int = 1, test: int = 1, seed: int = 0, random: bool = True) -> None:

        self.nsample = train + test

        if random:
            inds = range(len(self))

            np.random.seed(seed)
            inds = np.random.choice(inds, size=self.nsample, replace=False)

        train_confs = self.confs[inds[0:train]]
        train_force = self.force[inds[0:train]]

        test_confs = self.confs[inds[train:]]
        test_force = self.force[inds[train:]]

        return train_confs, train_force, test_confs, test_force

if __name__ == '__main__':

    filename = '../Data/Si_TB_300K.json'

    data = ForceConfs()
    data.read_json_data(filename)
    print(data)

    train_confs, train_force, test_confs, test_force = data.subsampling(30, 10, random=True)

    print(len(train_confs))
    print(len(train_force))
    print('END')
