import numpy as np

class MLCalculator_local:
    
    def __init__(self, fflist, nl, sym = True):
        self.nl = nl
        self.fflist = fflist
        self.sym = sym
        
    def get_potential_energy(self, atoms):
        return -1
        
    def get_forces(self, atoms):
        n = len(atoms)
        nl = self.nl
        fflist = self.fflist
        sym = self.sym
        
        nl.update(atoms)
        cell = atoms.get_cell()
        forces_ten = np.zeros((n, n, 3))
        
        for a in np.arange(n):
            ff = fflist[a]
            indices, offsets = nl.get_neighbors(a)
            offsets = np.dot(offsets, cell)

            for i, (a2, offset) in enumerate(zip(indices, offsets)):
                d = atoms.positions[a2] + offset - atoms.positions[a]
                forces_ten[a, a2, :] =  ff(d)

        print(sym)
        if sym:
            delta_f = (forces_ten + np.transpose(forces_ten, (1, 0, 2)))/2.
            print(np.mean(np.abs(delta_f)))
            forces_ten = forces_ten - delta_f
        else:
            None

        forces = np.einsum('nmd -> nd', forces_ten)

        return forces
