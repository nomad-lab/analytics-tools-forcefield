import numpy as np
import os.path
import datetime
from GP_custom_gen import GaussianProcess3 as GPvec

def rand_rotation_matrix(deflection=1.0, randnums=None):
    """
    Creates a random rotation matrix.
    
    deflection: the magnitude of the rotation. For 0, no rotation; for 1, competely random
    rotation. Small deflection => small perturbation.
    randnums: 3 random numbers in the range [0, 1]. If `None`, they will be auto-generated.
    """
    # from http://www.realtimerendering.com/resources/GraphicsGems/gemsiii/rand_rotation.c
    
    if randnums is None:
        randnums = np.random.uniform(size=(3,))
        
    theta, phi, z = randnums
    
    theta = theta * 2.0*deflection*np.pi  # Rotation about the pole (Z).
    phi = phi * 2.0*np.pi  # For direction of pole deflection.
    z = z * 2.0*deflection  # For magnitude of pole deflection.
    
    # Compute a vector V used for distributing points over the sphere
    # via the reflection I - V Transpose(V).  This formulation of V
    # will guarantee that if x[1] and x[2] are uniformly distributed,
    # the reflected points will be uniform on the sphere.  Note that V
    # has length sqrt(2) to eliminate the 2 in the Householder matrix.
    
    r = np.sqrt(z)
    Vx, Vy, Vz = V = (
        np.sin(phi) * r,
        np.cos(phi) * r,
        np.sqrt(2.0 - z)
        )
    
    st = np.sin(theta)
    ct = np.cos(theta)
    
    R = np.array(((ct, st, 0), (-st, ct, 0), (0, 0, 1)))
    
    # Construct the rotation matrix  ( V Transpose(V) - I ) R.
    
    M = (np.outer(V, V) - np.eye(3)).dot(R)
    return M


InDir = "DFT/Ni/500K"

forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))

confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))

lenc = len(forces)

print("Database lenght is: ", lenc)


### Subsampling from big database ###

ncal = 10
ntest = 1

ntot = ncal + ntest

ind = np.arange(lenc)

ind_ntot = np.random.choice(ind, size=ntot, replace=False)
ind_ncal = ind_ntot[0:ncal]
ind_ntest = ind_ntot[ncal:ntot]


print("Ntot Database lenght is: " ,len(ind_ntot))

### Spliting database in training/testing sets ###

tr_confs = confs[ind_ncal]
tr_forces = forces[ind_ncal]

tst_confs =  confs[ind_ntest]
tst_forces = forces[ind_ntest]


##########################


print("Initial shapes are ",np.shape(tr_confs[0]), np.shape(tr_forces), np.shape(tst_confs), np.shape(tst_forces))
print("One conf one force ", confs[0][ 0, :], forces[0])
print("Training Database lenght is: " ,len(tr_confs))
print("Testing Database lenght is: " ,len(tst_confs))

### Training and testing of the GP model ###

t0 = datetime.datetime.now()

### Training the GP model ###

# optimization methods: None, "fmin_l_bfgs_b", "Nelder-Mead"

t0train = datetime.datetime.now()
#nugget = 1e-20
# nugget = 1e-8
gp = GPvec( ker=[ 'id'], fvecs =['cov_mat'] ,nugget = 1e-8, theta0=np.array([1.]), sig =.5, bounds = ((0.1,10.),),
                        optimizer=  None, calc_error = False, eval_grad = False)

gp.fit(tr_confs, tr_forces)

tftrain = datetime.datetime.now()
print("Training computational time is", (tftrain-t0train).total_seconds())

### Testing the GP model ###

t0test = datetime.datetime.now()


#  Test Property 1, rotated configuration yelds rotated force


f_pred = gp.predict(tst_confs)                   # , err_pred 

print("Original prediction is ",  f_pred.T ,np.linalg.norm(f_pred))

rm = rand_rotation_matrix()
print("Rotated prediction is ", rm.dot(f_pred.T), np.linalg.norm(rm.dot(f_pred.T)))

new = np.reshape(np.einsum('ik, jk -> ji', rm, tst_confs[0]), (1, np.shape(tst_confs[0])[0], 3))
f_pred_new = gp.predict(new) 

print("Prediction for rotated configuration is ", f_pred_new.T, np.linalg.norm(f_pred_new))

f_pred = np.reshape(f_pred, (ntest, 3)) #f_pred.resize(ntest, 3)
tftest = datetime.datetime.now()


#  Test Property 2, random database transformation

print("Normal Database prediction is ", f_pred)

new_tr_confs = []
new_tr_forces = []

for i in np.arange(len(tr_forces)):
    rm = rand_rotation_matrix()
    new_tr_confs.append(np.einsum('ik, jk -> ji', rm, tr_confs[i]))
    new_tr_forces.append(rm.dot(tr_forces[i]))

gp.fit(new_tr_confs, new_tr_forces)

f_pred_new = gp.predict(tst_confs)     

print("Transformed Database prediction is ", f_pred_new)

