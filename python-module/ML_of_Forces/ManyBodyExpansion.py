import numpy as np
from scipy import interpolate

D = 3

class MBExp:
    """
    This class maps a trained gp process onto explicit basis functions
    """
    
    def __init__(self, gp):
        self.gp = gp
    
    ### interpolation ###
    
    def PW_L_fit(self, d0, df, Delta_d = 0.1):                  # Pairwise, linear fit
        gp = self.gp

        npoints = round((df-d0)/Delta_d)
        xgrid = np.linspace(d0, df, npoints)
        confs = np.zeros((npoints, 1, 3))
        confs[:, 0, 0] = xgrid
        forces = gp.predict(confs)
        if (forces[:, 1] > 0.).any():
            print("WARNING, force field is not exactly pairwise")
	
        xforces = forces[:, 0]
        interp = interpolate.interp1d(xgrid, xforces)
        self.interp = interp

    def PW_S_fit(self, d0, df, Delta_d = 0.1, k=3):                 # Pairwise, spline fit of order k
        gp = self.gp

        npoints = round((df-d0)/Delta_d)
        xgrid = np.linspace(d0, df, npoints)
        confs = np.zeros((npoints, 1, D))
        confs[:, 0, 0] = xgrid
        forces = gp.predict(confs)
        if (forces[:, 1] > 0.).any():
            print("WARNING, force field is not exactly pairwise")
	
        xforces = forces[:, 0]
        interp = interpolate.InterpolatedUnivariateSpline(xgrid, xforces, k=k, ext = 3)
        self.interp = interp
        
    ### prediction ###
    
    def pair_forces_scalars(self, ds):                  # Force magnitudes as a function of the distances ds
        interp = self.interp
        
        return interp(ds)        
	
    def pair_potential_scalars(self, d0, df, Delta_d = 0.1):                  # Potential Visualization
        interp = self.interp
        npoints = round((df-d0)/Delta_d)
        distances = np.linspace(df, d0, npoints)

        forces = interp(distances)  
        energies = np.cumsum(forces*Delta_d)
        return  - np.flipud(energies)      
    
    def pair_force_vector(self, r):                 # Force as a function of a vector
        interp = self.interp
        
        d = np.linalg.norm(r)
        r_hat = r/d	
        f_scalar = interp(d)
        f_vector = f_scalar * r_hat
        
        return f_vector

    def pair_force_conf(self, rs):                  # Force as a function of a configuration
        interp = self.interp

        ds = np.sqrt(np.einsum('nd, nd -> n', rs, rs))
        rs_hat = np.einsum('nd, n -> nd',rs, 1./ds)	
        fs_scalars = interp(ds)
        fs_vectors = np.einsum('n, nd -> d', fs_scalars, rs_hat)
        
        return fs_vectors
    
    def pair_forces_confs(self, confs):                 # Forces as a function of configurations
        interp = self.interp
        
        pair_force_conf = self.pair_force_conf
        forces = np.zeros((len(confs), D))
        for c in np.arange(len(confs)):
            rs = np.array((confs[c]))
            force = pair_force_conf(rs)
            forces[c] = force
            
        return forces

        

