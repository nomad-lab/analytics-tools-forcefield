from abc import ABCMeta, abstractmethod, abstractproperty


class GaussianProcess(metaclass=ABCMeta):


    # @abstractproperty
    # def kernel(self): pass

    @abstractmethod
    def fit(self): pass

    @abstractmethod
    def predict(self): pass



class Kernel(metaclass=ABCMeta):
    pass

class FeatureSpace(metaclass=ABCMeta):
    pass
