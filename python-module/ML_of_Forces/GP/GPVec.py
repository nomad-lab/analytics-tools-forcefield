from .baseclass import GaussianProcess

from typing import Optional
from typing import Union, List, Dict


class GPVec(GaussianProcess):

    def __init__(self,
                 kernel: Optional[str] = None,
                 datamodel=None):

        """
        sdfdsfdf
        :param kernel: Hello
        :type kernel: Optional[str]
        :param datamodel:
        """


        super().__init__()
        self.datamodel = datamodel  # type: Optional[str]
        self.kernel = kernel    # kernel functions

        pass

    def fit(self, X, Y):
        pass

    def predict(self): pass

    # def __repr__(self):
    #     return "%s(%r)" % (self.__class__, self.__dict__)

    def __str__(self):

        out = 'GaussianProcess:\n' +\
              '  datamodel: {}\n'.format(self.datamodel) +\
              '  kernel: {}\n'.format(self.kernel)
        return out


