import json


def read_json_data(filename):

    with open(filename, 'r') as jsonfile:
        data = json.load(jsonfile)

    return data['data']
