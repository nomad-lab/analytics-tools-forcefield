# from bokeh.plotting import figure, output_file, show
# output_file("test.html")
# p = figure()
# p.line([1, 2, 3, 4, 5], [6, 7, 2, 4, 5], line_width=2)
# show(p)


# from plotly import __version__
#
# from plotly.offline import download_plotlyjs, init_notebook_mode, plot, iplot
#
# print(__version__) # requires version >= 1.9.0
#
# from plotly.graph_objs import Scatter, Figure, Layout
#
# plot([Scatter(x=[1, 2, 3], y=[3, 1, 6])])

#
#
# import plotly
# from plotly.graph_objs import Scatter, Layout
#
# plotly.offline.plot({
#     "data": [Scatter(x=[1, 2, 3, 4], y=[4, 3, 2, 1])],
#     "layout": Layout(title="hello world")
# })

import plotly
from plotly.graph_objs import Scatter, Layout

plotly.offline.init_notebook_mode()

plotly.offline.iplot({
    "data": [Scatter(x=[1, 2, 3, 4], y=[4, 3, 2, 1])],
    "layout": Layout(title="hello world")
})