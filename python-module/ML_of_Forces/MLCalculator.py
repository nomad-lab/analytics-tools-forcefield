import numpy as np
from pathos.multiprocessing import ProcessingPool

class MLCalculator:
    
    def __init__(self, gp, nl):
        self.gp = gp
        self.nl = nl
        
    def get_potential_energy(self, atoms):
        return -1
        
    def get_forces(self, atoms):
        n = len(atoms)
        nl = self.nl
        gp = self.gp
        nl.update(atoms)
        cell = atoms.get_cell()
        confs = []
        
        for a in np.arange(n):
            indices, offsets = nl.get_neighbors(a)
            offsets = np.dot(offsets, cell)
            conf = np.zeros((len(indices), 3))
            for i, (a2, offset) in enumerate(zip(indices, offsets)):
                d = atoms.positions[a2] + offset - atoms.positions[a]
                conf[i] = d
            confs.append(conf)
        
        confs = np.array(confs)
        
        ### MULTIPROCESSING
        
        if __name__ == 'MLCalculator':
            nods = 1
            pool = ProcessingPool(nodes= nods)
            clist = [confs[i*n/nods:(i+1)*n/nods] for i in np.arange(nods)]
            result = np.array(pool.map(gp.predict,clist))

        forces = np.reshape(result, (n,3))

        return forces
