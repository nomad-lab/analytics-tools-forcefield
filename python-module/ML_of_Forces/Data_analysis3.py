import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import axes3d
import datetime
from scipy.stats import gamma
import os.path
from scipy.optimize import curve_fit
from GP_custom_gen import sim,  inv_sim, sim_48, sim_cut


# Problems: often when kinetic energy is plotted "Killed" error is displayed

### Functions ###


def dist( a, b, sig):
	
	first = 0.
	for i in np.arange(len(a)):
		for j in np.arange(len(a)):
			first += np.exp(-(a[i] - a[j])**2/(4*sig**2))
	first = 1./(len(a)**2)*first
	
	second = 0.
	for i in np.arange(len(a)):
		for j in np.arange(len(b)):
			second -= np.exp(-(a[i]-b[j])**2/(4*sig**2))
	second = 2./(len(a)*len(b))*second
	
	third = 0.
	for i in np.arange(len(b)):
		for j in np.arange(len(b)):
			third += np.exp(-(b[i]-b[j])**2/(4*sig**2))

	third = 1./(len(b)**2)*third
		
	return np.sqrt((first + second + third)/(2*(np.pi*sig**2)**(1./2.)))

def LJ_force(a, r_m, eps):                                                             # LJ repulsive force between atoms
    f = 0.
    for i in np.arange(len(a)):
        r = a[i]
        d = r.dot(r)
        f += - 12 *eps*((r_m/d)**12 + (r_m/d)**6)*(r/d)
    return f

def harmonic_force(a, rm, amp):                                                             # LJ repulsive force between atoms
    f = np.array([0., 0.])
    for i in np.arange(len(a)):
        r = a[i]
        d = np.sqrt((np.dot(r, r)))
        if d <= 1.5:            
            r_hat = r/d
            f += amp*(d - rm)*r_hat
    #print(f)
    return f
    
def gaussian(mean, st_dev, x):
	return 1/np.sqrt(2*np.pi*st_dev**2)*np.exp(-(x-mean)**2/(2*st_dev**2))

def find_Df(probs, Dfs, dDf, chi_tar):
    Df_tar = 0.
    chi_dum = 0.
    chi_dumm1 = 0.
    i = 0
    im1 = -1
    while chi_dum <= chi_tar:
        chi_dum += probs[i]*dDf
        if im1 >= 0:
            chi_dumm1 += probs[im1]*dDf
        #print i, im1, chi_dum, chi_dumm1
        i += 1
        im1 += 1
        
    #slope = np.absolute((probs[i]-probs[im1])/(Dfs[i]-Dfs[im1]))            
    #dDf_tar =  (probs[im1]+ np.sqrt(probs[i-1]**2 - 2*slope*(chi_tar-chi_dumm1)))/slope  # for better estimate, however small "slope" in denominator makes it instable
    dDf_tar = (chi_tar-chi_dumm1)/probs[im1]
    return Dfs[im1]+dDf_tar 
        
def cfunc(errors, avab_forces, ncals ,chi):
    cfunc = np.zeros(len(avab_forces))
    for idx, ncal in enumerate(ncals):
        scalederrors = errors[idx]/avab_forces[idx]
        nbins = 555
        prange = .4
        hist = np.histogram(scalederrors,bins = nbins, normed = 1., range=[0, prange])
        probs = hist[0]
        dDf = prange/nbins
        Dfs = hist[1]+dDf/2.
        Dfs = np.delete(Dfs, -1)
        cfunc[idx] = find_Df(probs, Dfs, dDf, chi)
    return cfunc

def lear_func(ncals, errors, normed_forces):
    lfunc = np.zeros((len(ncals), 2))
    for i in np.arange(len(lfunc)):
        scalederrors = errors[i]#/np.mean(normed_forces[i])
        lfunc[i] =  np.array([ncals[i],np.mean(scalederrors)])
    return lfunc

def COM(a):
    com = np.array([0., 0.])
    for i in np.arange(len(a)):
        r = a[i]
        com += r
    return com
    
def feat_vecs(a, fvecs = "COM"):
    f_vecs = np.zeros((16, 2))
    if fvecs is "COM":
        f_vecs = np.zeros((16, 2))
        com = COM(a)
        n_com = np.sqrt(com.dot(com))
        rot = np.array([[np.cos(np.pi/2.), -np.sin(np.pi/2.)], [np.sin(np.pi/2.),  np.cos(np.pi/2.)]]) 
        comr = rot.dot(com)
        f_vecs[0] = com
        f_vecs[1] =  comr
    elif fvecs is 'LJ':
        f_vecs = np.zeros((1, 2))
        f_vecs[0] = LJ_force(a, 1, 1)
    elif fvecs is 'LJs':
        LL = 4
        f_vecs = np.zeros((LL, 2))
        idx = 0
        for eps in np.linspace(0.1, 1., 2):
            for rm in np.linspace(0.1, 1., 2):
                f_vecs[idx] = LJ_force(a, rm, eps)
                idx += 1
    elif fvecs is 'harmonic':           # note LJ equilibrium amplitude is 72
        LL = 4
        f_vecs = np.zeros((LL, 2))
        idx = 0
        for amp in np.linspace(62, 82, 2):
            for rm in np.linspace(0.9, 1.1, 2):
                f_vecs[idx] = harmonic_force(a, rm, amp)
                #print(f_vecs[idx])
    return f_vecs
                
rot = np.array([[np.cos(np.pi/2.), -np.sin(np.pi/2.)], [np.sin(np.pi/2.),  np.cos(np.pi/2.)]]) 

### What to caculate ###

histo = 0			# 1: plots PDF for speed, position, kinetic and potential energies
timesall = 0		# 1: plots time series of positions, potential and kinetic energies of all particles
timesmean = 0		# 1: calculates and plots time series of mean quantities
compl1Tplot = 0			# 1: calculates complexity function for one system and plots it
compl_comp =  0         # 1: calculates complecity function for several systems (e.g. different T) and plots them together
compl_comp_wstats =  0         # 1: same as previous with added stats over several runs
learning_curves = 0            # 1: generates a learning curve
learning_curves_wstats = 0    # 1: generates a learning curve from more than one run
learning_curves_wstats2 = 0    # 1: learning curve from more than one run at different T
dens_calc = 0		# 1: calculates pdf of distance between configurations
dens_plot = 0		# 1: plots a pdf of the previously calculated "distances.npy"	
force_hist = 0
err_hist = 0
dens_relerr = 1                    # 1: Density of Relative error plot
principal_CA = 0                 # 1: Principal Component Analysis of the Data
kernel_PCA = 0                      # 1: Principal Component Analysis in the Feature space induced by a certain kernel function 
conf_sims = 0                          #1: plots configurations and similarity functions on them
confplt3d = 0
build_f1f2_database = 0               #1: from forces database builts components database
build_distances_database = 0
 
### Global Variables ###

n = 30                 # total number of particles
r_m = 1.               # roughly position of the minimum
eps = 1.             # depth of the potential well
r_cut = 3.25*r_m            # cutoff distance (about 2.5 sigma = 2.5*1.22r_m)
x0 = 30.                     # amplitude of the range of initial positions
x_size = 31.5
dt = 0.0002               # time-step (it has to be roughly 0.0001 for the simulaiton to work)
every = 250.               # print every "every" timesteps
t = 1.                    # total simulation time
steps = np.round(t/dt)    # total number of steps
temp = 0.1             # temperature (note, mass and friction coefficients were set to one)


### Plots ###

InDir = "DFT/Fe/500Ks/"

if timesall==1 or histo == 1 or timesmean ==1:
	
    ### Import Data ###
    
    forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))
    
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))
    
    positions = np.asarray(np.load(os.path.join(InDir,"positionst.npy")))
    
    #energytot = np.asarray(np.load(os.path.join(InDir,"energytott.npy")))
    
    #energypot = np.asarray(np.load(os.path.join(InDir,"energypott.npy")))
    
    #energykin = np.asarray(np.load(os.path.join(InDir,"energykint.npy")))
    
    print("Forces/Confs Database legth ", len(forces))
    print("Dynamics Database legth", len(positions[0, :]))
    
    if timesall == 1:
        fs = 15
  
        for i in np.arange(len(positions[:, 0, 0])):
           plt.scatter(positions[i, 0,0], positions[i, 1,0], color = 'r', s = 300)
           plt.scatter(positions[i, 0,:], positions[i, 1,:])
        
        plt.show()
        spart = 17
  
        plt.scatter(positions[spart, 0, 0], positions[spart, 1, 0], color = 'r', s = 300)
        plt.scatter(positions[spart, 0, :], positions[spart, 1, :])   
        plt.ylabel('x', fontsize = fs)
        plt.xlabel('y', fontsize = fs)
        plt.show()
  
        """
        for i in np.arange(n):
            plt.plot(energypot[i, :])
        plt.ylabel('Potential Energies')
        plt.show()
  
  
        for i in np.arange(n):
            plt.plot(energykin[i, :])
        plt.ylabel('Kinetic Energies')
        plt.show()
  
        for i in np.arange(n):
            plt.plot(energytot[i, :])
        plt.ylabel('Total Energies')
        plt.show()"""
  
        print "mean F_x is", np.mean(forces[:, 0]), "mean F_y is ",  np.mean(forces[:, 1])
        print np.shape(forces)
        plt.scatter(forces[:, 0], forces[:, 1])
        plt.ylabel('F_y', fontsize = fs)
        plt.xlabel('F_x', fontsize = fs)
        plt.show()
  
        plt.plot(forces[:, 0], 'ro', alpha = 0.2)
        plt.ylabel('F_x', fontsize = fs)
        plt.xlabel('Time-steps', fontsize = fs)
        plt.show()
        """
        print np.shape(confs)
        plt.plot(confs, 'ro', alpha =0.2)
        plt.ylabel('Confs', fontsize = fs)
        plt.xlabel('Time-steps', fontsize = fs)
        plt.show()"""
    
    if histo == 1:
		fs = 15
		ls = fs
		
		U0 = - 2.0620117 
		
		
		plt.hist(np.sqrt(2*energykin[:, steps-200000:steps-1].flatten()),95, normed = 1., range=[0, 1], label = 'Estimated')
		plt.tick_params(axis = "both", which = "major", labelsize = ls)
		x = np.linspace(0, 1, 100)
		def pv(v):
			return 2*np.exp(-v**2/(2*temp))/np.sqrt(2*np.pi*temp)
		plt.plot(x, pv(x), 'r-',linewidth = 2.5, label = "Theoretical (exact)")
		plt.legend()
		plt.xlabel("velocity norm $(v)$", fontsize = fs)
		plt.ylabel("$p(v)$",fontsize = fs)
		plt.savefig("speed.png", dpi = 200)
		plt.show()
		
		plt.hist(energykin[:, steps-200000:steps-1].flatten(),95, normed = 1.,range=[0, 0.2], label = 'Estimated')
		plt.tick_params(axis = "both", which = "major", labelsize = ls)
		plt.xlim(0, 0.18)
		x = np.linspace(0.0007, 0.2, 100)
		def pk(k):
			return np.exp(-k/temp)/np.sqrt(k*np.pi*temp)
		plt.plot(x, pk(x), 'r-',linewidth = 2.5, label = "Theoretical (exact)")
		plt.legend()
		plt.xlabel("kinetic energy $(K)$", fontsize = fs)
		plt.ylabel("$p(K)$", fontsize = fs)
		plt.savefig("ekin.png", dpi = 200)
		plt.show()
		
		plt.hist(positions[1, steps-200000:steps-1].flatten(),13, normed = 1., label = 'Estimated')
		plt.tick_params(axis = "both", which = "major", labelsize = ls)
		x = np.linspace(0.0007, 0.25, 100)

		plt.legend()
		plt.xlabel("position $(x)$", fontsize = fs)
		plt.ylabel("$p(x)$", fontsize = fs)
		plt.savefig("pos.png", dpi = 200)
		plt.show()
		
		plt.hist(energypot[:, steps-200000:steps-1].flatten(),105, normed = 1.,range=[-2.08, -1.4], label = 'Estimated')
		plt.tick_params(axis = "both", which = "major", labelsize = ls)
		x = np.linspace(-2.053, -1.6, 100)
		def pU(U):
			return np.exp(-(U-U0)/(2*temp))/np.sqrt(2*(U-U0)*np.pi*temp)
		plt.plot(x, pU(x), 'r-',linewidth = 2.5, label = "Theoretical (approximate)")
		plt.legend()
		plt.xlim(-2.1, -1.6)
		plt.xlabel("potential energy $(U)$", fontsize = fs)
		plt.ylabel("$p(U)$", fontsize = fs)
		plt.savefig("epot.png", dpi = 200)
		plt.show()
	
    if timesmean ==	1:
       fs = 15
       
       EtotG = np.asarray(np.load(os.path.join(InDir,"EtotGt.npy")))
    
       EpotG = np.asarray(np.load(os.path.join(InDir,"EpotGt.npy")))
    
       EkinG = np.asarray(np.load(os.path.join(InDir,"EkinGt.npy")))

       #W_thermG = np.asarray(np.load(os.path.join(InDir,"W_thermGt.npy")))
        
       print("Temperature is roughly", np.mean(EkinG[np.round(len(EkinG)/2.): ]))
       
       plt.plot(EpotG, 'b', label = "Potential")
       plt.plot(EkinG, 'r', label = "Kinetic")
       plt.plot(EtotG+3.18, 'g', label = "Total")
       plt.legend()
       plt.ylabel('Mean Energies',  fontsize = fs)
       plt.show()
       

       """
       fig, ax1 = plt.subplots()
    
       ax1.plot(mean_en[:, 0], 'b-',  linewidth = .7)
       ax1.set_xlabel('timesteps',  fontsize = fs)
       # Make the y-axis label and tick labels match the line color.
       ax1.set_ylabel('potential energy', color='b',  fontsize = fs)
       for tl in ax1.get_yticklabels():
           tl.set_color('b')
    
    
       ax2 = ax1.twinx()
       s2 = np.sin(2*np.pi*t)
       ax2.plot(mean_en[:, 1], 'r-', linewidth = .7)
       ax2.set_ylabel('kinetic energy', color='r',  fontsize = fs)
       for tl in ax2.get_yticklabels():
           tl.set_color('r')
       plt.show()
       """

if force_hist == 1:
   fs = 15
   forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))
   forces = np.sqrt((forces**2).sum(2))
   forces = forces.reshape(500*64)
   print(np.shape(forces))
   #plt.hist(np.hstack((forces[:, 0], np.hstack((forces[:, 1], forces[:, 2])))), 80, alpha = 0.3, normed = 1)
   plt.hist(forces, 60, alpha = 0.3, normed = 1)
   plt.xlabel("Force component, "+r'$ F_{x,y,z}$' +' ' +r'$(\rm{eV/ \AA})$', fontsize = fs) 
   plt.ylabel(r'$p(F)$', fontsize = fs)
   plt.show()
   
if err_hist == 1:
   fs = 15
   forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))
   forces = np.sqrt((forces**2).sum(2))
   forces = forces.reshape(500*64)
   
   runs = ["2nd","3rd", "4th", "5th"]
   errors = np.load(os.path.join(InDir,"1st/ComplN/errors.npy"))[4, :]
   errors2 = np.load(os.path.join(InDir,"1st/Compl48S/errors.npy"))[4, :]
   for run in runs:
      ed = np.load(os.path.join(InDir,run + "/ComplN/errors.npy"))[4, :]
      ed2 = np.load(os.path.join(InDir,run + "/Compl48S/errors.npy"))[4, :]
      errors = np.hstack((errors,ed))
      errors2 = np.hstack((errors2,ed2))
   
   print(np.shape(errors))
   #plt.hist(forces, 60, alpha = 0.25, normed = 1, color = 'r',label = "Force moduli")
   normed_value = 1
   hist, bins = np.histogram(forces, bins=60, density=True)
   widths = np.diff(bins)
   hist *= normed_value
   plt.bar(bins[:-1], hist, widths, alpha = 0.25, color = 'r',label = "Force moduli")
   normed_value = 0.05
   hist *= normed_value
   plt.bar(bins[:-1], hist, widths, alpha = 0.25, color = 'g',label = "Force moduli")
   normed_value = 0.05  
   hist, bins = np.histogram(errors2, bins=10, density=True)
   widths = np.diff(bins)
   hist *= normed_value
   #plt.bar(bins[:-1], hist, widths, alpha = 0.35, color = 'g', label = r'$O_{48}$'+" (Rotations, Reflection)")
   
   normed_value = 0.5
   hist, bins = np.histogram(errors, bins=25, density=True)
   widths = np.diff(bins)
   hist *= normed_value
   #plt.bar(bins[:-1], hist, widths, alpha = 0.35, color = 'b', label = "No symmetries")
   
   #plt.hist(errors, 25, alpha = 0.35, normed = 1, label = "No symmetries")
   #plt.hist(errors2, 10, alpha = 0.35, normed = 1, label = r'$O_{48}$'+" (Rotations, Reflection)")

   plt.xlabel(r'$ |\Delta F |, F $' +' '+r'$(\rm{eV/ \AA})$', fontsize = fs) 
   plt.ylabel(r'$p(|\Delta F|), p(F)$', fontsize = fs)
   plt.legend()
   plt.xlim(0, 2.7)
   plt.show()
   

if dens_relerr == 1:
  fs, ls = 18, 15
  bins = np.linspace(0.0005, 3.2, 30)
  path = "DFT/Ni/500K/ComplRuns/"
  normed_forces = np.load(os.path.join(path, "1st/SO3/normed_forces.npy")).T
  nf2 = np.load(os.path.join(path, "2nd/SO3/normed_forces.npy")).T
  nf3 = np.load(os.path.join(path, "3rd/SO3/normed_forces.npy")).T
  nf4 = np.load(os.path.join(path, "4th/SO3/normed_forces.npy")).T
  normed_forces = np.vstack((normed_forces, nf2, nf3, nf4))
  hist, bins = np.histogram(normed_forces, bins=bins, density=True)
  widths = np.diff(bins)
  errors = np.load(os.path.join(path,"1st/SO3/errors.npy")).T
  e2 = np.load(os.path.join(path, "2nd/SO3/errors.npy")).T
  e3 = np.load(os.path.join(path, "3rd/SO3/errors.npy")).T
  e4 = np.load(os.path.join(path, "4th/SO3/errors.npy")).T
  errors =  np.vstack((errors, e2, e3, e4))
  print(len(errors))
  relerr = errors/normed_forces
  inds = np.digitize(normed_forces, bins)
  mean = [relerr[inds == i].mean() for i in range(1, len(bins))]
  plt.plot(normed_forces, relerr, 'ro', alpha = 0.2, label = "Data")
  plt.plot( bins[:-1], mean, label = "Mean")
  plt.xlabel(r'$ F $' +' '+r'$(\rm{eV/ \AA})$', fontsize = fs)
  plt.ylabel(r'$ \Delta F / F $', fontsize = fs)
  plt.xlim(0.0005, 3.2)
  plt.ylim(0, 5)
  plt.legend(fontsize = ls)
  #plt.show()
  
  fig, ax1 = plt.subplots()
  ax1.bar(bins[:-1], hist, widths, alpha = .85, color = "#54acc4",label = "Force Modulus Probability")
  ax1.bar(bins[:-1], hist*mean, widths, alpha = 0.85, color = "#dcdcdc",label = "Relative Error Density")
  print("normalisation of FMP is ", np.sum((hist*widths)[:-1]))
  print("normalisation of RED is ", np.sum((hist*mean*widths)[:-1]))
  left, bottom, width, height = [0.6, 0.35, 0.25, 0.25]
  ax2 = fig.add_axes([left, bottom, width, height])
  ax1.set_xlabel(r'$ f $' +' '+r'$(\rm{eV/ \AA})$', fontsize = fs)
  ax1.set_ylabel(r'$ p(f), \,  RED(f) $', fontsize = fs)
  ax1.set_xlim(0.0005, 3.5)
  ax1.legend(fontsize = ls)
  
  ax2.plot(range(6)[::-1], color='green')
  plt.show()

if compl1Tplot == 1:
     
    errors = np.load(os.path.join(InDir,"errors.npy"))
    avab_forces= np.load(os.path.join(InDir,"avab_forces.npy"))
    l = 6#len(avab_forces)
    colours = iter(plt.cm.rainbow(np.linspace(0, 1, len(avab_forces))))
    cfunca = np.zeros((l, 5)) # 0 is ncal, 1 is mean error, 2 is st_dev, 3 is entropy, 4 is Df such that integral probability = chi_tar
    
    for idx, ncal in enumerate([ 10,  20,  40,  80, 160, 320]):
        c = next(colours)
        scalederrors = errors[idx]/avab_forces[idx]
        nbins = 30
        prange = 2.
        hist = np.histogram(scalederrors,bins = nbins, normed = 1., range=[0, prange])
        probs = hist[0]
        dDf = prange/nbins
        Dfs = hist[1]+dDf/2.
        Dfs = np.delete(Dfs, -1)
        plt.plot(Dfs, probs, '-o', color = c, label = "$\mathcal{N}=$"+str((ncal)))
        cfunca[idx, 0] = ncal
        cfunca[idx, 1] = np.dot(Dfs, probs*dDf)
        cfunca[idx, 2] = 2*np.sqrt(np.dot((Dfs-cfunca[idx, 1])**2, probs*dDf))
        cfunca[idx, 3] = - np.dot(probs*dDf,np.log(probs*dDf)) 
        cfunca[idx, 4] = find_Df(probs, Dfs, dDf, 0.9)
        
    fs = 15
    plt.figure(1)
    plt.xlabel("Relative Error " + r'$ ( |\Delta F | / \overline{  |F|} } ) $' , fontsize = fs)
    plt.ylabel("$p(\Delta F)$", fontsize = fs)
    plt.legend()
    plt.savefig("hists.png", dpi = 250 )
    plt.figure(2)

    plt.plot(cfunca[:,1],cfunca[:, 0], '-o', label = "Mean error $(\overline{\Delta F})$")
    #plt.plot(cfunca[:,2],cfunca[:, 0], '-o', label = "Std. deviation $(\sigma (\Delta F))$")
    #plt.plot(cfunca[:,3],cfunca[:, 0], '-o', label = "Entropy $(S)$")
    plt.plot(cfunca[:,4],cfunca[:, 0], '-o', label = "Target error $(\Delta F \mid \, \chi = 0.9)$")
    plt.xlabel("Error/Information Measure", fontsize = fs)
    plt.ylabel("$\mathcal{N}$", fontsize = fs)
    plt.legend()
    plt.savefig("cfunc.png", dpi = 250 )
    plt.show()

if compl_comp == 1:
    systems = ["0.025", "0.05", "0.1"]
    complexities = []
    ncals = [ 100,  200,300,  400,  500, 600, 700, 800, 900, 1000]
    chi = 0.9
    colours = ["b", "r", "k"]#iter(plt.cm.rainbow(np.linspace(0, 1, len(systems)+3)))
    powerfit = True
    fs = 15
    
    for i, T in enumerate(systems):
        InDir = "/home/k1192572/Downloads/GeneralNN/TB/BenzeneP2_Telec/T=" + T+"/complAs"
        errors = np.load(os.path.join(InDir,"errors.npy"))
        avab_forces= np.load(os.path.join(InDir,"avab_forces.npy"))
        cf = cfunc(errors, avab_forces, ncals,  chi)
        complexities.append(cf)
        c = colours[i]
        lb = "T = "+T+", $\overline{|F|} = $" + str(np.round(avab_forces[i], 1))
        plt.plot(cf, ncals, 'o', color = c , linewidth = 1.5) # label = lb
        if powerfit == True:
            def power(Df, a, b):
                return (Df**a)*b
            m_q = np.polyfit(np.log(cf), np.log(ncals), 1)
            x = np.linspace(0.0005, np.amax(cf), 100)
            plt.plot(x, power(x, m_q[0], np.exp(m_q[1])), color = c, label = lb)
            print("For T =" + T + " power is " + str(m_q[0]) )
        
        

    plt.legend()
    ymin, ymax = 100, np.amax(ncals)
    xmin, xmax=  0,0.16
    #plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.ylabel(r'$\mathcal{N}_{\mathbf{S}}(\Delta F, \chi = 0.9)$', fontsize = fs)
    plt.xlabel("Relative Error "+r'$ (|\Delta F|/\overline{|F|})$', fontsize = fs)
    plt.show()
    
if compl_comp_wstats == 1:
    systems = ["0.025", "0.05", "0.1"]
    runs = ["", "2"]# "3", "4", "5"]
    complexities = []
    ncals = [ 100,  200,300,  400,  500, 600, 700, 800, 900, 1000]
    chi = 0.9
    colours = ["b", "r", "k"]#iter(plt.cm.rainbow(np.linspace(0, 1, len(systems)+3)))
    powerfit = False
    fs = 15
    
    for i, T in enumerate(systems):
        cfr = np.zeros((len(ncals), len(runs)))
        for j, r in enumerate(runs):
            InDir = "/home/k1192572/Downloads/GeneralNN/TB/BenzeneP2/T=" + T+"/complAs" + r
            errors = np.load(os.path.join(InDir,"errors.npy"))
            avab_forces= np.load(os.path.join(InDir,"avab_forces.npy"))
            cf = cfunc(errors, avab_forces, ncals,  chi)
            cfr[:, j] = cf
         
        cf = np.mean(cfr, axis = 1)
        cfst = np.std(cfr, axis = 1)
        c = colours[i]
        lb = "T = "+T+", $\overline{|F|} = $" + str(np.round(avab_forces[i], 1))
        plt.errorbar(cf, ncals, xerr= cfst, color = c, fmt='-o', linewidth = 1.2,  label = lb)
        
        if powerfit == True:
            def power(Df, a, b):
                return (Df**a)*b
            m_q = np.polyfit(np.log(cf), np.log(ncals), 1)
            x = np.linspace(0.0005, np.amax(cf), 100)
            plt.plot(x, power(x, m_q[0], np.exp(m_q[1])), color = c, label = lb)
            print("For T =" + T + " power is " + str(m_q[0]) )
        
        

    plt.legend(loc = 2)
    ymin, ymax = np.amin(ncals), np.amax(ncals)
    xmin, xmax=  0, np.amax(cf)
    plt.xlim(xmin, xmax)
    plt.ylim(ymin, ymax)
    plt.ylabel(r'$\mathcal{N}_{\mathbf{S}}(\Delta F, \chi = 0.9)$', fontsize = fs)
    plt.xlabel("Relative Error "+r'$ (|\Delta F|/\overline{|F|})$', fontsize = fs)
    plt.show()
  
if learning_curves == 1:
    subfolders = ["N", "6S", "AllS"]
    ncals = [ 10,  20,  40,  80, 160, 320]
    colours = ["b", "r", "k"]
    fs = 15
    for idx, sub in enumerate(subfolders):
        InDir = "/home/k1192572/Downloads/2Dlearning/triang/T=0.05/10_320/Compl" + sub
        errors = np.load(os.path.join(InDir,"errors.npy"))
        avab_forces= np.load(os.path.join(InDir,"avab_forces.npy"))
        lfun = lear_func(ncals, errors, avab_forces)
        c = colours[idx]
        lb = sub
        plt.plot(lfun[:, 0], lfun[:, 1], '-o', color = c, label = lb)
    plt.legend()
    plt.ylabel("Relative Error "+r'$ (|\Delta F|/\overline{|F|})$', fontsize = fs)
    plt.xlabel(r'$\mathcal{N}$', fontsize = fs)
    plt.show()

if learning_curves_wstats == 1:
    subfolders = ["NSE", "O48SE"]#"48S","O3"]
    runs =["1st", "2nd"]#,"3rd", "4th"]
    ncals = [5, 10, 20,  40,  80, 160, 320]
    colours = ["b", "r", "k", 'c']
    fs, ls = 18, 15
    #labels = ["No symmetries", r'$O_{24}$' + " (Rotations)", r'$O_{48}$' + " (Rotations, Reflections)"]
    labels = ["No symmetries",r'$SO(3)$']#,r'$O_{48}$' + " (Rotations, Reflections)",  r'$O(3)$']
    for idx, sub in enumerate(subfolders):
        lfuncs = np.zeros((len(runs),len(ncals)))
        for idr, run in enumerate(runs):
            InDir = "DFT/Fe/500K/ComplRuns/"+run +'/' + sub
            errors = np.load(os.path.join(InDir,"errors.npy"))[0:]                          # NOTE: I am eliminating N=10 data point
            normed_forces= np.load(os.path.join(InDir,"normed_forces.npy"))[0:]
            lfuncs[idr] = lear_func(ncals, errors, normed_forces)[:, 1]
        mean = np.mean(lfuncs, axis = 0)
        stdev = np.std(lfuncs, axis = 0)
        c = colours[idx]
        lb = labels[idx]
        plt.errorbar(ncals, mean, yerr= stdev, color = c, fmt='-o', linewidth = 1.2,  label = lb)
   
    ax = plt.subplot()
    ax.set_xscale("log", nonposx='clip')
    #ax.set_yscale("log", nonposy='clip')
    ax.set_xticks(ncals, minor = False)   
    ax.set_xticklabels(ncals, minor = False)
    plt.legend(fontsize = ls)
    plt.ylabel("Absolute Error "+r'$ |\Delta F |$' +' ' +r'$(\rm{eV/ \AA})$', fontsize = fs) 
    plt.xlabel("Training points " +r'N', fontsize = fs)
    plt.show()


if learning_curves_wstats2 == 1:
    #["N", '24S', "48S"]
    subfolders = ["N", 'SO3']
    runs =["1st", "2nd","3rd", "4th"]
    ncals = [10, 20,  40,  80, 160, 320]
    colours = ["b", "k", 'c']
    fs, ls = 18, 15
    labels = ["No symmetries, 500K",r'$SO(3)$'+", 500K"]#["No symmetries, 500K", r'$O_{24}$' + ', 500K', r'$O_{48}$' + ", 500K"]
    #labels = ["No symmetries",r'$SO(3)$']#,r'$O_{48}$' + " (Rotations, Reflections)",  r'$O(3)$']
    for idx, sub in enumerate(subfolders):
        lfuncs = np.zeros((len(runs),len(ncals)))
        for idr, run in enumerate(runs):
            # "Si_TB/500K/ComplRuns/"+run +'/' + sub
            InDir = "DFT/Ni/500K/ComplRuns/"+run +'/' + sub
            errors = np.load(os.path.join(InDir,"errors.npy"))[0:]                          # NOTE: I am eliminating N=10 data point
            normed_forces= np.load(os.path.join(InDir,"normed_forces.npy"))[0:]
            lfuncs[idr] = lear_func(ncals, errors, normed_forces)[:, 1]
        mean = np.mean(lfuncs, axis = 0)
        stdev = np.std(lfuncs, axis = 0)
        c = colours[idx]
        lb = labels[idx]
        plt.errorbar(ncals, mean, yerr= stdev, color = c, fmt='-o', linewidth = 1.2,  label = lb)
    
    # ["N", "48S"]
    subfolders = ["N", "SO3"]
    colours = ["b", "k", 'c']
    labels = ["No symmetries, 1700K",r'$SO(3)$' + ", 1700K"]#["No symmetries, 1000K",r'$O_{48}$' + ", 1000K"]
    #labels = ["No symmetries",r'$SO(3)$']#,r'$O_{48}$' + " (Rotations, Reflections)",  r'$O(3)$']
    for idx, sub in enumerate(subfolders):
        lfuncs = np.zeros((len(runs),len(ncals)))
        for idr, run in enumerate(runs):
            # "Si_TB/1000K/ComplRuns/"+run +'/' + sub
            InDir = "DFT/Ni/1700K/ComplRuns/"+run +'/' + sub
            errors = np.load(os.path.join(InDir,"errors.npy"))[0:]                          # NOTE: I am eliminating N=10 data point
            normed_forces= np.load(os.path.join(InDir,"normed_forces.npy"))[0:]
            lfuncs[idr] = lear_func(ncals, errors, normed_forces)[:, 1]
        mean = np.mean(lfuncs, axis = 0)
        stdev = np.std(lfuncs, axis = 0)
        c = colours[idx]
        lb = labels[idx]
        plt.errorbar(ncals, mean, yerr= stdev, color = c, fmt='-o', linewidth = 1.2,  label = lb, ls = 'dashed')
    ax = plt.subplot()
    ax.set_xscale("log", nonposx='clip')
    #ax.set_yscale("log", nonposy='clip')
    ax.set_xticks(ncals, minor = False)   
    ax.set_xticklabels(ncals, minor = False)
    plt.legend( fontsize = ls)
    plt.ylabel("Absolute Error "+r'$ |\Delta F |$' +' ' +r'$(\rm{eV/ \AA})$', fontsize = fs) 
    plt.xlabel("Training points " +r'N', fontsize = fs)
    plt.show()


if dens_calc == 1:
    nconfs = 150
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))
    confs = confs[-nconfs:]
    sig = 1.
    dists = []
    t0 = datetime.datetime.now()
    for i in np.arange(nconfs):		# total number of calculations will be (N*N-N)/2, scales quadratically.
        for j in np.arange(i):	# i+1 also considers the distance of a configuration from itself (=zero)
           
            distance = sim(confs[i], confs[j], sig)
            #if distance < 0.00118 and distance > 0.001175:
            if distance < 0.001425 and distance > 0.00142:
               print(np.shape(confs[i]), np.shape(confs[j]))
               #None
            
            dists.append(distance)
    tf = datetime.datetime.now()
    print("Computational time is", (tf-t0).total_seconds())
    np.save(os.path.join(InDir,"distances.npy"), dists)
    print("Distance database is ", len(dists))
	
if dens_plot == 1:
   dists = np.load(os.path.join(InDir,"distances.npy"))
   print(len(dists))
   dists = dists#np.sqrt(2 +2* dists)

   mean = np.mean(dists)
   mode = 0#stats.mode(dists)
   alpha, loc, beta = gamma.fit(dists)
   st_dev = np.std(dists)
   print("mean, mode and st_dev are: ", mean, mode,  st_dev, alpha, loc, beta)
   #x = np.linspace(0, 0.10, 100)
   plt.hist(dists, 100, alpha = 0.75, normed = 1., label = "Estimated")
   #plt.plot(x, gamma.pdf(x, alpha, loc, beta),'r-',linewidth = 2.5, label = "Gamma pdf, MAL fit")
   fs = 15
   plt.xlabel("Distance $(d)$", fontsize = fs)
   plt.ylabel("$p(d)$", fontsize = fs)
   #plt.xlim([0, 0.10])
   plt.legend()
   plt.savefig("hist_dist.png", dpi = 200)
   plt.show()
   #plt.plot(dists, '.')
   
   #plt.show()

if principal_CA ==1 :
    from sklearn.decomposition import PCA
    fs = 15
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))[0:200]
    forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))[0:200]
    confs.resize((200*28, 3))
    #forces = np.reshape(forces, (len(forces), 1))
    print("confs and forces shapes are ", np.shape(confs), np.shape(forces))
    #fc = np.concatenate((forces, confs), axis = 1)
    pca = PCA(n_components = 3)             # 'mle' finds optimal number of PC (computationally heavier)
    print(np.shape(confs))
    confs2 = np.zeros((3200,10))
    print(confs[0, 1])
    pca.fit(confs)
    confsp = pca.transform(confs)
    plt.plot(pca.explained_variance_ratio_)
    plt.xlabel("Principal Component", fontsize = fs)
    plt.ylabel("Variance along PC", fontsize = fs)
    plt.show()


    plt.plot(confsp[:, 0], confsp[:, 1], 'ro')
    plt.xlabel("First PCA", fontsize = fs)
    plt.ylabel("Second PCA", fontsize = fs)
    plt.show()
    

    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsp[:, 0], confsp[:, 1],  confsp[:, 2])
    plt.show()
    
    pca.fit(fc)
    fc = pca.transform(fc)
    plt.plot(fc[:, 0], fc[:, 1], 'ro')
    plt.xlabel("First PCA", fontsize = fs)
    plt.ylabel("Second PCA", fontsize = fs)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsp[:, 0], confsp[:, 1], forces)
    plt.show()
    
if kernel_PCA ==1 :
    from sklearn.decomposition import KernelPCA
    fs = 15
    n_use = 200
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))
    forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))
    confs = confs[-n_use:]
    forces = forces[-n_use:]
    #forces = np.reshape(forces, (len(forces), 3*64))
    confs = np.reshape(confs,(n_use, 28*3 ))
    #confs.resize((n_use, 28*3))
    print("confs and forces shapes are ", np.shape(confs), np.shape(forces))
    #fc = np.concatenate((forces, confs), axis = 1)
    print(np.shape(confs))
    def dist_metric(a, b):
        ar = np.reshape(a, (28, 3))
        br = np.reshape(b, (28, 3))
        d = (sim(ar, br, 0.5) + sim(ar, -br, 0.5))/2.
        #d =inv_sim(ar, br, 0.5) 
        #print(d)
        return d

    kpca = KernelPCA(n_components = 5, kernel = dist_metric)
    confsk = kpca.fit_transform(confs)
    print(np.shape(confsk), confsk[:, 0])
    plt.plot(kpca.lambdas_)
    plt.xlabel("Principal Component", fontsize = fs)
    plt.ylabel("Variance along PC", fontsize = fs)
    plt.show()
    
    plt.plot(confsk[:, 0], confsk[:, 1], 'ro')
    plt.xlabel("First PCA", fontsize = fs)
    plt.ylabel("Second PCA", fontsize = fs)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsk[:, 0], confsk[:, 1],  confsk[:, 2])
    plt.show()
    """
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsk[:, 0], confsk[:, 1], forces, 'ro')
    ax.set_xlabel("First PC", fontsize = fs)
    ax.set_ylabel("Second PC", fontsize = fs)
    ax.set_zlabel("Force", fontsize = fs)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsk[:, 0], confsk[:, 1], confsk[:, 2], 'ro')
    ax.set_xlabel("First PC", fontsize = fs)
    ax.set_ylabel("Second PC", fontsize = fs)
    ax.set_zlabel("Third PC", fontsize = fs)
    plt.show()
    
    fig = plt.figure()
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(confsk[:, 0], confsk[:, 1], confsk[:, 2], c= forces)
    ax.set_xlabel("First PC", fontsize = fs)
    ax.set_ylabel("Second PC", fontsize = fs)
    ax.set_zlabel("Third PC", fontsize = fs)
    plt.show()
    """
    
if conf_sims == 1:
    
    fs = 15
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy"))) 
    idxa, idxb = np.random.random_integers(len(confs)), np.random.random_integers(len(confs))
    a, b = confs[idxa], confs[idxb]
    r_c = 1.8
    thetas = np.linspace(0, 2*np.pi, 360)
    plt.plot(sim_sq_cut(a, b, .25, thetas, r_c), 'b-')
    plt.plot(sim_sq(a, b, .25, thetas), 'r-')
    plt.ylabel("Squared overlap integral")
    plt.xlabel("Rotation angle ")
    plt.show()  
    
    plt.scatter(a[:, 0], a[:, 1], color = 'b', label="First conf. ", s= 100, alpha = 0.3)
    plt.scatter(b[:, 0], b[:, 1], color = 'r', label="Second conf. ", s= 100, alpha = 0.3)
    v1 = feat_vecs(a, "LJs")[0]
    v2 = feat_vecs(a, "LJs")[3]
    v3 = feat_vecs(a, "harmonic")[0]
    print(v3)
    v1, v2 ,v3= v1/np.sqrt(v1.dot(v1)),v2/np.sqrt(v2.dot(v2)),v3/np.sqrt(v3.dot(v3))
    print(v1, v2, np.sqrt(v1.dot(v1)), np.sqrt(v2.dot(v2)))
    plt.quiver(0, 0, v1[0], v1[1], color = 'k', scale = 1, scale_units = 'xy')
    plt.quiver(0, 0, v2[0], v2[1], color = 'brown', scale = 1, scale_units = 'xy')
    plt.quiver(0, 0, v3[0], v3[1], color = 'r', scale = 1, scale_units = 'xy')
    plt.xlabel("x", fontsize = fs)
    plt.ylabel("y", fontsize = fs)
    plt.legend( fontsize = fs)
    plt.show()
    
if confplt3d == 1:
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy"))) 
    c = confs[1]
    c2 = confs[2]
    print(len(c), len(c2), sim(c, c2, 1.))
    fig = plt.figure(1)
    ax = fig.add_subplot(111, projection = '3d')
    ax.scatter(c[:, 0], c[:, 1],  c[:, 2],  c='r')
    ax.scatter(c2[:, 0], c2[:, 1],  c2[:, 2], c='b')
    plt.show()
    
    
if build_f1f2_database == 1:
    
    forces =  np.asarray(np.load(os.path.join(InDir,"forcest.npy"))) [1000:1100]
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy"))) [1000:1100]
    size = len(forces)
    print(size)
    coms = np.zeros((size, 2))
    n_coms = np.zeros(size)
    f1s = np.zeros(size)
    f2s = np.zeros(size)
    for i in np.arange(size):
        com = harmonic_force(confs[i], 1, 72)#LJ_force(confs[i])
        
        n_com = np.sqrt(com.dot(com))
        coms[i,:] = com/n_com
        rcom = rot.dot(coms[i])
        n_coms[i] = n_com
        f1s[i] = np.dot(forces[i], coms[i])
        f2s[i] = np.dot(forces[i], rcom)
        #print(forces[i], n_coms[i], f1s[i])
    
    np.save(os.path.join(InDir,"f1st.npy"), f1s)
    np.save(os.path.join(InDir,"f2st.npy"), f1s)
    plt.plot(forces[:, 0], 'ro', alpha = 0.25)
    plt.show()
    plt.plot(f1s, 'ro',  alpha = 0.25)
    plt.show()
    plt.plot(f2s, 'ro',  alpha = 0.25)
    plt.show()

if build_distances_database == 1:
    ker = "MAX_CUT"
    lenc = 400
    sig = 0.5
    r_c = 1.8
    INV_SIM_mat = np.zeros((lenc, lenc))
    INV_SIM_SQ_mat = np.zeros((lenc, lenc))
    MAX_SIM__mat = np.zeros((lenc, lenc))
    INV_SIM_SQ_CUT_mat = np.zeros((lenc, lenc))
    MAX_SIM_CUT_mat = np.zeros((lenc, lenc))
    forces =  np.asarray(np.load(os.path.join(InDir,"forcest.npy"))) 
    confs = np.asarray(np.load(os.path.join(InDir,"confst.npy"))) 
    
    ind = np.arange(lenc)
    ind_ntot = np.random.choice(ind, size=lenc, replace=False)
    
    confs_ntot = confs[ind_ntot]
    forces_ntot = forces[ind_ntot]
    np.save(os.path.join(InDir,"confs_subt_" + ker + ".npy"),  confs_ntot)
    np.save(os.path.join(InDir,"forces_subt_" + ker + ".npy"),  forces_ntot)
    t0 = datetime.datetime.now()
    if ker == 'MAX':
        
        for i in np.arange(lenc):
            for j in np.arange(i+1):
                MAX_SIM_mat[i, j] = max_sim(confs_ntot[i], confs_ntot[j], sig)
                
        MAX_SIM_mat = MAX_SIM_mat + MAX_SIM_mat.T - np.diag(np.diag(MAX_SIM_mat))
        np.save(os.path.join(InDir,"MAX_SIM_matt.npy"),  MAX_SIM_mat)
        
    elif ker == "INV":
        for i in np.arange(lenc):
            for j in np.arange(i+1):
                INV_SIM_SQ_mat[i, j] = inv_sim_sq(confs_ntot[i], confs_ntot[j], sig)
        
        INV_SIM_SQ_mat = INV_SIM_SQ_mat +INV_SIM_SQ_mat.T - np.diag(np.diag(INV_SIM_SQ_mat))
        np.save(os.path.join(InDir,"INV_SIM_SQ_matt.npy"),  INV_SIM_SQ_mat)
        
    
    if ker == 'MAX_CUT':
        
        for i in np.arange(lenc):
            for j in np.arange(i+1):
                MAX_SIM_CUT_mat[i, j] = max_sim_cut(confs_ntot[i], confs_ntot[j], sig, r_c)
                
        MAX_SIM_CUT_mat = MAX_SIM_CUT_mat + MAX_SIM_CUT_mat.T - np.diag(np.diag(MAX_SIM_CUT_mat))
        np.save(os.path.join(InDir,"MAX_SIM_CUT_matt.npy"),  MAX_SIM_CUT_mat)

    elif ker == "INV_CUT":
        for i in np.arange(lenc):
            for j in np.arange(i+1):
                INV_SIM_SQ_mat[i, j] = inv_sim_sq_cut(confs_ntot[i], confs_ntot[j], sig, r_cut)
        
        INV_SIM_SQ_CUT_mat = INV_SIM_SQ_CUT_mat +INV_SIM_SQ_CUT_mat.T - np.diag(np.diag(INV_SIM_SQ_CUT_mat))
        np.save(os.path.join(InDir,"INV_SIM_SQ_CUT_matt.npy"),  INV_SIM_SQ_CUT_mat)
        
    tf = datetime.datetime.now()
    print("Computational time is", (tf-t0).total_seconds())
