import numpy as np
import matplotlib.pyplot as plt
from numpy import linalg as LA
import random
from mpl_toolkits.mplot3d import axes3d
from GP_custom_gen import GaussianProcess3 as GPvec
import os.path
import datetime

### Importing data from simulation ###

InDir = "DFT/Fe/vac"

forces = np.asarray(np.load(os.path.join(InDir,"forcest.npy")))
"""
n = len(forces[0, :, 0])
print("Number of particles is ", n)
s = len(forces[:, 0, 0])
print("Number of samples is ", s)
forces = np.resize(forces,(n*s, 3))
"""

confs = np.asarray(np.load(os.path.join(InDir,"confst.npy")))
print("Force ans confs shapes are ", np.shape(forces), np.shape(confs))
print("Typical configuration length is ", len(confs[0]))
lenc = len(forces)

print("Database lenght is: ", lenc)
	
def compl(ncal, ntest):
    """
    Inputs: 'ncal' is number of training point, 'ntest' is the number of testing points
    Outputs: array with probability of each error, array with bin limits
    """
    
    ### Subsampling from original database ###	
    
    ntot = ncal + ntest
    
    ind = np.arange(lenc)
    
    ind_ntot = np.random.choice(ind, size=ntot, replace=False)
    
    confs_ntot = confs[ind_ntot]
    forces_ntot = forces[ind_ntot]
    
    print("Ntot Database lenght is: " ,len(forces_ntot))
    
    ### Spliting database in training/testing sets ###
    
    tr_confs = confs_ntot[0:ncal]
    tr_forces = forces_ntot[0:ncal]
    
    tst_confs =  confs_ntot[ncal:]
    tst_forces = forces_ntot[ncal:]
    
    print("Training Database lenght is: " ,len(tr_confs))
    print("Testing Database lenght is: " ,len(tst_confs))
    
    ### Training the GP model ###
    t0train = datetime.datetime.now()
    gp = GPvec( ker=[ 'id'], fvecs =[ 'cov_mat'] ,nugget = 1e-10, theta0=np.array([0.6]), sig = 0.5, bounds = ((1, 10),),
                        optimizer=  None, calc_error = False, eval_grad = False)
    
    gp.fit(tr_confs, tr_forces)
    tftrain = datetime.datetime.now()
    print("Training computational time is", (tftrain-t0train).total_seconds())
    ### Testing the GP model ###
    t0test = datetime.datetime.now()
    f_pred= gp.predict(tst_confs)                   # , err_pred 
    f_pred.resize(ntest, 3)
    tftest = datetime.datetime.now()
    print("Testing computational time is", (tftest-t0test).total_seconds())
    
    ### Getting errors made in testing ###
    errors = np.zeros(len(tst_forces))
    for i in np.arange(len(errors)):
        errors[i] = np.linalg.norm(f_pred[i]-tst_forces[i])
    tst_norms = np.sqrt((tst_forces**2).sum(1))
    return errors, tst_norms#np.mean(np.absolute(tst_norms))

errors = []
avab_forces = []

for ncal in [10, 20, 40, 80, 160, 320]:
    err, avab_force = compl(ncal, 1000)
    errors.append(err)
    avab_forces.append(avab_force)

OutDir ="DFT/Fe/vac/ComplRuns_rc5.2/2nd/SO3"

np.save(os.path.join(OutDir,"errors.npy"), errors)
np.save(os.path.join(OutDir,"normed_forces.npy"), avab_forces)


"""
plt.plot(tst_forces, tst_forces)
plt.plot(tst_forces, f_pred, 'ro')
plt.ylabel('prediction')
plt.xlabel('real value')
plt.show()
"""









