"""
Gaussian process regression module. Inspired by sklearn.gaussian_process module and stripped out naked to the bare minimum
"""

from __future__ import print_function, division
from scipy import linalg as LA
from scipy.linalg import cholesky, cho_solve
import scipy as sp
import numpy as np
import scipy.spatial.distance as spdist
from scipy.optimize import fmin_l_bfgs_b
from scipy.optimize import minimize
from scipy.spatial.distance import cdist
from scipy.special import iv
from sklearn.preprocessing import normalize

from transforms3d import axangles as tr

MACHINE_EPSILON = sp.finfo(sp.double).eps

D = 3                                                 # number of dimensions of inpud data

ref = np.array([[1, 0], [0,  -1]])                    # 2D reflection matrix

ref3d = np.array([[-1, 0, 0],[0, 1, 0], [0, 0,  1]])  # 3D reflection matrix

rten48 = np.zeros((48, 3, 3))                         # 3D octahedral symmetry matrices

def rten48_init():
   rten48[0] = np.array([[1,0,0],
                                   [0,1,0],
                                   [0,0,1]])
                              
   rten48[1] = np.array([[-1,0,0],
                                   [0,-1,0],
                                   [0,0,1]])
                              
   rten48[2] = np.array([[-1,0,0],
                                   [0,1,0],
                                   [0,0,-1]])
                              
   rten48[3] = np.array([[1,0,0],
                                   [0,-1,0],
                                   [0,0,-1]])
                              
   rten48[4] = np.array([[0,0,1],
                                   [1,0,0],
                                   [0,1,0]])
                              
   rten48[5] = np.array([[0,0,1],
                                 [-1,0,0],
                                 [0,-1,0]])
                              
   rten48[6] = np.array([[0,0,-1],
                                   [-1,0,0],
                                   [0,1,0]])
                              
   rten48[7] = np.array([[0,0,-1],
                                   [1,0,0],
                                   [0,-1,0]])
                              
   rten48[8] = np.array([[0,1,0],
                                   [0,0,1],
                                   [1,0,0]])
                              
   rten48[9] = np.array([[0,-1,0],
                                   [0,0,1],
                                   [-1,0,0]])
                              
   rten48[10] = np.array([[0,1,0],
                                    [0,0,-1],
                                    [-1,0,0]])
                              
   rten48[11] = np.array([[0,-1,0],
                                    [0,0,-1],
                                    [1,0,0]])
                              
   rten48[12] = np.array([[0,1,0],
                                    [1,0,0],
                                    [0,0,-1]])
                              
   rten48[13] = np.array([[0,-1,0],
                                    [-1,0,0],
                                    [0,0,-1]])
                              
   rten48[14] = np.array([[0,1,0],
                                    [-1,0,0],
                                    [0,0,1]])
                              
   rten48[15] = np.array([[0,-1,0],
                                    [1,0,0],
                                    [0,0,1]])
                              
   rten48[16] = np.array([[1,0,0],
                                   [0,0,1],
                                   [0,-1,0]])
                              
   rten48[17] = np.array([[-1,0,0],
                                    [0,0,1],
                                    [0,1,0]])
                              
   rten48[18] = np.array([[-1,0,0],
                                    [0,0,-1],
                                    [0,-1,0]])
                              
   rten48[19] = np.array([[1,0,0],
                                    [0,0,-1],
                                    [0,1,0]])
                              
   rten48[20] = np.array([[0,0,1],
                                    [0,1,0],
                                    [-1,0,0]])
                              
   rten48[21] = np.array([[0,0,1],
                                     [0,-1,0],
                                     [1,0,0]])
                              
   rten48[22] = np.array([[0,0,-1],
                                      [0,1,0],
                                      [1,0,0]])
                              
   rten48[23] = np.array([[0,0,-1],
                  [0,-1,0],
                  [-1,0,0]])
                     
   # up to here only rotations (24), from here also reflections
   
   rten48[24] = np.array([[-1,0,0],
                  [0,-1,0],
                  [0,0,-1]])
      
   rten48[25] = np.array([[1,0,0],
                  [0,1,0],
                  [0,0,-1]])
             
   rten48[26] = np.array([[1,0,0],
                  [0,-1,0],
                  [0,0,1]])                                        

   rten48[27] = np.array([[-1,0,0],
                  [0,1,0],
                  [0,0,1]])
      
   rten48[28] = np.array([[0,0,-1],
                  [-1 ,0,0],
                  [0,-1,0]])                        
      
   rten48[29] = np.array([[0,0,-1],
                  [1,0,0],
                  [0,1,0]])                        
      
   rten48[30] = np.array([[0,0,1],
                  [1,0,0],
                  [0,-1,0]])                        
      
   rten48[31] = np.array([[0,0,1],
                  [-1,0,0],
                  [0,1,0]])                        
      
   rten48[32] = np.array([[0,-1,0],
                  [0,0,-1],
                  [-1,0,0]])                        
      
   rten48[33] = np.array([[0,1,0],
                  [0,0,-1],
                  [1,0,0]])                        
      
   rten48[34] = np.array([[0,-1,0],
                  [0,0,1],
                  [1,0,0]])                        
      
   rten48[35] = np.array([[0,1,0],
                  [0,0,1],
                  [-1,0,0]])
          
   rten48[36] = np.array([[0,-1,0],
                  [-1,0,0],
                  [0,0,1]])                          
      
   rten48[37] = np.array([[0,1,0],
                  [1,0,0],
                  [0,0,1]])                          
      
   rten48[38] = np.array([[0,-1,0],
                  [1,0,0],
                  [0,0,-1]])                          
      
   rten48[39] = np.array([[0,1,0],
                  [-1,0,0],
                  [0,0,-1]])     
      
   rten48[40] = np.array([[-1,0,0],
                  [0,0,-1],
                  [0,1,0]])                          
      
   rten48[41] = np.array([[1,0,0],
                  [0,0,-1],
                  [0,-1,0]])                          
      
   rten48[42] = np.array([[1,0,0],
                  [0,0,1],
                  [0,1,0]])                          

   rten48[43] = np.array([[-1,0,0],
                  [0,0,1],
                  [0,-1,0]])  
      

   rten48[44] = np.array([[0,0,-1],
                  [0,-1,0],
                  [1,0,0]])                          
                 
   rten48[45] = np.array([[0,0,-1],
                  [0,1,0],
                  [-1,0,0]])                          
      
   rten48[46] = np.array([[0,0,1],
                  [0,-1,0],
                  [-1,0,0]])                          
      
   rten48[47] = np.array([[0,0,1],
                  [0,1,0],
                  [1,0,0]])                          
                                                                     
rten48_init()

def rot(angle):
    rotm = np.array([[np.cos(angle), np.sin(angle)], [-np.sin(angle),  np.cos(angle)]]) 
    return rotm

def cart2pol(r):                                      #  cartesian to polar transformation
    rho = np.sqrt(r.dot(r))
    phi = np.arctan2(r[1], r[0])
    return np.array([rho, phi])

def f_cut(r, r_c):                                    # standard cutoff function
    result = 0.5 * (np.cos(np.pi*r/r_c) + 1.)
    return result

def sim_sq(a, b, sig, theta):                         # squared overlap, used for debugging
    la = len(a)
    lb = len(b)
    
    sim = 0.
    for i in np.arange(la):
        for j in np.arange(lb):
            for l in np.arange(la):
                for m in np.arange(lb):
                    val = 0.
                    ri, rj = a[i], b[j]
                    rl, rm = a[l], b[m]
                    ris, rjs = ri.dot(ri), rj.dot(rj)
                    rls, rms = rl.dot(rl), rm.dot(rm)
                    rip, rjp = cart2pol(ri), cart2pol(rj)
                    rlp, rmp = cart2pol(rl), cart2pol(rm)
                    thetaij = rip[1] - rjp[1] 
                    thetalm = rlp[1] - rmp[1] 
                    C =  np.exp(- (ris + rjs + rls + rms )/(4.*sig**2))
                    gamma_c = 1./(2*sig**2)*(np.dot(ri, rj) + np.dot(rl, rm))
                    gamma_s = 1./(2*sig**2)*(np.sqrt(ris*rjs)*np.sin(thetaij) + np.sqrt(rls* rms)*np.sin(thetalm))
                    gamma =  np.sqrt(gamma_c**2 + gamma_s**2)
                    thetaijlm = np.arctan2(gamma_s,gamma_c) #np.arctan2(gamma_s**2,gamma_c**2)
                    val = C * np.exp(gamma*np.cos(thetaijlm + theta))
                    sim += val
                    
    sim = sim/(la*lb*4.*np.pi*sig**2)**2
    return sim

def inv_sim_sq(a, b, sig):                            # squared overlap integrated over 2D rotations
    la = len(a)
    lb = len(b)
    sigsq = sig*sig
    ris, rls = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rl = np.sqrt(ris), np.sqrt(rls)
    rirj = np.outer(ri,ri)
    rlrm = np.outer(rl, rl)
    risPrjs = ris[:, None] + ris[:, None].T
    rlsPrms = rls[:, None] + rls[:, None].T
    Cij, Clm = np.exp(-risPrjs/(4*sigsq)), np.exp(- rlsPrms/(4*sigsq))
    Cijlm = np.einsum('ij, lm -> ijlm', Cij, Clm)
    costhetaij = np.einsum('id,jd -> ij', a, a)/rirj
    costhetalm = np.einsum('ld,md -> lm', b, b)/rlrm
    sinthetaij = np.cross(a[ None], a[:, None])/rirj 
    sinthetalm = np.cross(b[ None], b[:, None])/rlrm 

    gammac_ijlm =  ((rirj*costhetaij)[:, :, None, None] + \
            np.transpose((rlrm*costhetalm)[:, :, None, None], (2, 3, 0, 1)))
    gammas_ijlm =  ((rirj*sinthetaij)[:, :, None, None] + \
            np.transpose((rlrm*sinthetalm)[:, :, None, None], (2, 3, 0, 1)))
    
    gamma_ijlm = 1./(2.*sig**2)  *  np.sqrt(gammac_ijlm**2 + gammas_ijlm**2)
    valijlm = Cijlm * iv(0, gamma_ijlm) 
        
    val = valijlm.sum()

    return val/(la*lb*4.*np.pi*sig**2)**2
    
def inv_sim_sq_ker(a, b, sig):
    la = len(a)
    lb = len(b)
    sigsq = sig*sig
    ris, rls = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rl = np.sqrt(ris), np.sqrt(rls)
    rirj = np.outer(ri,ri)
    rlrm = np.outer(rl, rl)
    risPrjs = ris[:, None] + ris[:, None].T
    rlsPrms = rls[:, None] + rls[:, None].T
    Cij, Clm = np.exp(-risPrjs/(4*sigsq)), np.exp(- rlsPrms/(4*sigsq))
    Cijlm = np.einsum('ij, lm -> ijlm', Cij, Clm)

    costhetaij = np.einsum('id,jd -> ij', a, a)/rirj
    costhetalm = np.einsum('ld,md -> lm', b, b)/rlrm
    sinthetaij = np.cross(a[ None], a[:, None])/rirj 
    sinthetalm = np.cross(b[ None], b[:, None])/rlrm 

    gammac_ijlm = 1./(2.*sig**2)  * ((rirj*costhetaij)[:, :, None, None] + \
            np.transpose((rlrm*costhetalm)[:, :, None, None], (2, 3, 0, 1)))
    gammas_ijlm = - 1./(2.*sig**2)  * ((rirj*sinthetaij)[:, :, None, None] + \
            np.transpose((rlrm*sinthetalm)[:, :, None, None], (2, 3, 0, 1)))

    gamma_ijlm =   np.sqrt(gammac_ijlm**2 + gammas_ijlm**2)
    valijlm = Cijlm *( iv(1, gamma_ijlm))
        
    thetaijlm = np.arctan2(gammac_ijlm, gammas_ijlm)
    #thetaijlm2 = np.arctan(gammac_ijlm/gammas_ijlm)
    print(thetaijlm.sum(), thetaijlm[0, 0, 0, 0])
    val = valijlm.sum()/(la*lb*4.*np.pi*sig**2)**2
    valcos = (valijlm*np.cos(thetaijlm)).sum()/(la*lb*4.*np.pi*sig**2)**2
    valsin = (valijlm*np.sin(thetaijlm)).sum()/(la*lb*4.*np.pi*sig**2)**2
    result = + np.identity(D)*valsin
    result[0, 1] =   - valcos
    result[1, 0] =  + valcos
    print(valsin, valcos)
    return result
    
def simC(a, b, sig):                     # overlap between configurations
    la = len(a)
    lb = len(b)
    simij = cdist(a, b, 'sqeuclidean')
    simij = 2*sig/(4*sig**2 + simij)/np.pi
    sim = simij.sum()/(la*lb)
    return sim
    
def sim(a, b, sig):                     # overlap between configurations
    la = len(a)
    lb = len(b)
    print
    simij = cdist(a[:, 0:3], b[:, 0:3], 'sqeuclidean')
    simij = np.exp(-simij/(4*sig**2))
    
    elecij = cdist(a[:, 3, None], b[:, 3, None], 'sqeuclidean')
    elecij = np.exp(-elecij/(2))
    simij = elecij*simij
    sim = simij.sum()/(la*lb*(2*(np.pi*sig*sig)**0.5)**D)
    return sim
   
def sim_48(a, b, sig):                 
    syms = 48
    simo = 0.
    for sym in np.arange(syms):
        rm = rten48[sym]
        simo += sim(a, np.einsum('ik, jk -> ji', rm, b), sig)
    return simo*(1./syms) 
    
def cyclic_2D(a, b, sig):              # 2D cyclic group kernel

    simo = np.zeros((2,2))
    nangs = 6
    for ang in  np.linspace(0, 2*np.pi * (1.- 1./nangs), nangs):
        rm = rot(ang)
        simo += rm * sim(a, np.einsum('ik, jk -> ji', rm, b), sig)
    return simo/nangs

def dihedral_2D(a, b, sig):            # 2D dihedral group kernel

    simo = np.zeros((2,2))
    n = 6
    for ang in  np.linspace(0, 2*np.pi * (1.- 1./n), n):
        rm = rot(ang)
        simo += rm * sim(a, np.einsum('ik, jk -> ji', rm, b), sig)
        simo += rm.dot(ref) * sim(a, np.einsum('ik, jk -> ji', rm,  np.einsum('ik, jk -> ji', ref, b)), sig)
    return simo/(2*n)
 
def cov_sim2D(a, b, sig):              # SO(2) covariant kernel                                                    
    la = len(a)
    lb = len(b)

    sigsq = sig*sig
    ris, rjs = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rj = np.sqrt(ris), np.sqrt(rjs)
    rirj = np.outer(ri,rj)

    risPrjs = ris[:, None] + rjs[:, None].T

    Cij = np.exp(-risPrjs/(4*sigsq))
    costhetaij = np.einsum('id,jd -> ij', a, b)/rirj
    sinthetaij = np.cross(a[None], b[:,None]).T/rirj 
    gammaij = rirj/(2*sigsq)
    A = (Cij*iv(1, gammaij)*costhetaij).sum()
    B = (Cij*iv(1, gammaij)*sinthetaij).sum()
    simo =  np.array([[A,B],[-B, A]])/(la*lb*(2*(np.pi*sigsq)**0.5)**D)
    return simo

def cov_sim3D(a, b, sig):              # SO(3) covariant kernel
    la = len(a)
    lb = len(b)
    sigsq = sig*sig
    ao, bo = a, b
    a, b = a[:, 0:3], b[:, 0: 3]
    ris, rjs = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rj = np.sqrt(ris), np.sqrt(rjs)
    rirj = np.outer(ri,rj)
    risPrjs = ris[:, None] + rjs[:, None].T
    Cij = np.exp(-risPrjs/(4*sigsq))
    
    
    elecij = cdist(ao[:, 3, None], bo[:, 3, None], 'sqeuclidean')
    #print(ao[:, 3, None], bo[:, 3, None])
    #print(cdist(ao[:, 3, None], bo[:, 3, None], 'sqeuclidean'))
    elecij = np.exp(-elecij/(.005))
    #print(elecij)
    Cij = elecij*Cij
    
    
    
    gammaij = rirj/(2*sigsq)

    Iu2 = Cij*((gammaij*np.cosh(gammaij) - np.sinh(gammaij))/gammaij**2)
    Iumat = np.zeros((la, lb, 3, 3))
    Iumat[:, :, 2, 2] = Iu2

    zaxis1, zaxis2 = np.zeros((la, 3)), np.zeros((lb, 3))
    zaxis1[:, 2], zaxis2[:, 2] = 1., 1.

    m1axis, m2axis = np.cross(a, zaxis1), np.cross(b, zaxis2)
    m1angle, m2angle = np.arccos(np.einsum('id, id -> i',a, zaxis1)/ri), np.arccos(np.einsum('id, id -> i',b, zaxis2)/rj)

    M1T = np.einsum( 'abi -> iba' ,tr.axangle2mat2(m1axis, m1angle))
    M2 = np.einsum( 'abi -> iab' ,tr.axangle2mat2(m2axis, m2angle))

    result = np.einsum('iab, ijbc , jcd -> ad',M1T, Iumat, M2)

    simo =  result/(la*lb*(2.*(np.pi*sigsq)**0.5)**D)

    if a is b:
        simo = (simo + simo.T)/2.
    return simo
    
def cov_sim3D_C(a, b, sig):              # SO(3) covariant kernel
    la = len(a)
    lb = len(b)
    sigsq = sig*sig
    ris, rjs = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rj = np.sqrt(ris), np.sqrt(rjs)
    rirj = np.outer(ri,rj)
    risPrjs = ris[:, None] + rjs[:, None].T
    Cij = np.sqrt((4*sigsq + risPrjs - 2*rirj)/(4*sigsq + risPrjs + 2*rirj)) * (4*sigsq + risPrjs - 2*rirj)*np.pi

    Iu2 = 2*sig/Cij
    
    Iumat = np.zeros((la, lb, 3, 3))
    Iumat[:, :, 2, 2] = Iu2

    zaxis1, zaxis2 = np.zeros((la, 3)), np.zeros((lb, 3))
    zaxis1[:, 2], zaxis2[:, 2] = 1., 1.

    m1axis, m2axis = np.cross(a, zaxis1), np.cross(b, zaxis2)
    m1angle, m2angle = np.arccos(np.einsum('id, id -> i',a, zaxis1)/ri), np.arccos(np.einsum('id, id -> i',b, zaxis2)/rj)

    M1T = np.einsum( 'abi -> iba' , tr.axangle2mat2(m1axis, m1angle))
    M2 = np.einsum( 'abi -> iab' , tr.axangle2mat2(m2axis, m2angle))

    result = np.einsum('iab, ijbc , jcd -> ad',M1T, Iumat, M2)

    simo =  result/(la*lb)
    
    if a is b:
        simo = (simo + simo.T)/2.
    return simo
    
    
def cov_sim3D_cond(a, b, sig):            # tests for the SO(3) cov kern                                                     
    la = len(a)
    lb = len(b)
    sigsq = sig*sig
    ris, rjs = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rj = np.sqrt(ris), np.sqrt(rjs)
    rirj = np.outer(ri,rj)
    risPrjs = ris[:, None] + rjs[:, None].T
    
    Cij1 = np.exp(-risPrjs/(4*sigsq))
    gammaij = rirj/(2*sigsq)
    #print(gammaij)
    Cij2 = np.exp(- risPrjs/(2*rirj))*np.exp(-gammaij/2.)
    print(Cij1.sum(), Cij2.sum())
    Iu2 = Cij2*((gammaij*np.cosh(gammaij) - np.sinh(gammaij))/gammaij**2)
    expansion = (-1./gammaij**2 + 1./gammaij + np.exp(-2*gammaij)*(1./gammaij + 1./gammaij**2))/2.
    Iu22 = Cij2*expansion/2.
    #print(Iu2.sum(), Iu22.sum())
    Iumat = np.zeros((la, lb, 3, 3))
    Iumat[:, :, 2, 2] = Iu2

    zaxis1, zaxis2 = np.zeros((la, 3)), np.zeros((lb, 3))
    zaxis1[:, 2], zaxis2[:, 2] = 1., 1.
    m1axis, m2axis = np.cross(a, zaxis1), np.cross(b, zaxis2)
    m1angle, m2angle = np.arccos(np.einsum('id, id -> i',a, zaxis1)/ri), np.arccos(np.einsum('id, id -> i',b, zaxis2)/rj)
 
    M1T = np.einsum( 'abi -> iba' ,tr.axangle2mat2(m1axis, m1angle))
    M2 = np.einsum( 'abi -> iab' ,tr.axangle2mat2(m2axis, m2angle))

    result = np.einsum('iab, ijbc , jcd -> ad',M1T, Iumat, M2)

    simo =  result/(la*lb*(2.*(np.pi*sigsq)**0.5)**D)

    if a is b:
        simo = (simo + simo.T)/2.
    return simo


def cov_sim2D_ref(a, b, sig):                # 2D integral over reflections                                            
    la = len(a)
    lb = len(b)

    sigsq = sig*sig
    ris, rjs = ((a**2).sum(1)), ((b**2).sum(1))
    ri, rj = np.sqrt(ris), np.sqrt(rjs)
    rirj = np.outer(ri,rj)

    risPrjs = ris[:, None] + rjs[:, None].T

    Cij = np.exp(-risPrjs/(4*sigsq))
    costhetaij = np.einsum('id,jd -> ij', a, b)/rirj
    sinthetaij = np.cross(a[None], b[:,None]).T/rirj 
    gammaij = rirj/(2*sigsq)
    A = (Cij*iv(1, gammaij)*costhetaij).sum()
    B = - (Cij*iv(1, gammaij)*sinthetaij).sum()
    simo =  np.array([[A,B],[B, -A]])/(la*lb*(2*(np.pi*sigsq)**0.5)**D)
    return simo
    
def cov_sim48(a, b, sig, theta):             # octahedral covariant kernel
    la, lb = len(a), len(b)
    
    simgij = np.array( [cdist(a, np.einsum('ik, jk -> ji', rten48[sym], b), 'sqeuclidean') for sym in np.arange(48)] )
    simgij = np.exp(-simgij/(4*sig**2))
    simg = np.einsum('gij -> g', simgij)/(la*lb*(2*(np.pi*sig*sig)**0.5)**D)
    simo =  np.einsum(' kij , k -> ij ', rten48, (1+simg)**theta)
    
    return simo*(1./48.)
    
def inv_sim(a, b, sig):                      # overlap integrated over 2D rotations  
    la = len(a)
    lb = len(b)
    
    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    
    amod = np.atleast_2d(amod).T              
    bmod = np.atleast_2d(bmod).T
    rimrjsq = cdist(amod, bmod, 'sqeuclidean')
    rirj = np.outer(amod, bmod)
    simij = np.exp(-(rimrjsq + 2*rirj)/(4*sig*sig))*iv(0, rirj/(2*sig*sig))
    sim = simij.sum()/(la*lb*(2*(np.pi*sig*sig)**0.5)**D)

    return sim

def sim_cut(a, b, sig, r_c):                     # overlap between configurations, with cutoff
    la = len(a)
    lb = len(b)
    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    fi = f_cut(amod, r_c)
    fj = f_cut(amod, r_c)
    fifj = np.outer(fi, fj)
    simij = cdist(a, b, 'sqeuclidean')
    simij = np.exp(-simij/(4*sig**2)) * fifj
    sim = simij.sum()/(la*lb*(2*(np.pi*sig*sig)**0.5)**D)
    #print(max(amod), f_cut(max(amod), r_c))
    return sim

def LJ_force(a, rm, eps):                        # LJ force

    ds = np.sqrt((a**2).sum(1))
    term = 12*eps/ds*((rm/ds)**12-(rm/ds)**6)/ds

    f = np.einsum("i, id -> d",term, a)
    return f

def LJ_force_params(a, rms, epss):               # LJ over grid of parameters                                                 

    ds = np.sqrt((a**2).sum(1))
    epsds = np.outer(epss, 1./ds**2)
    rmds = np.outer(rms, 1./ds)
    
    term = 12*epsds*((rmds)**12-(rmds)**6)

    f = np.einsum("ji, id -> jd",term, a)
    return f

def harmonic_force(a, rm, amp):                  # harmonic force

    ds = np.sqrt((a**2).sum(1))
    ds[ds > 1.5] = rm
    term = amp*(ds - rm)/ds
    f = np.einsum("i, id -> d",term, a)
    return f

def integr_LJs_mat_modegen(a, b, eps_mod, rm_mod ,sig_eps, sig_rm):     # LJ with integrated mode

    logr_m = np.log(rm_mod)

    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    outer = np.outer(amod , bmod)
    term24 = np.exp(24.*logr_m+ 312.*sig_rm**2)/(outer**12)
    term18 = - np.exp(18.*logr_m+ 180.*sig_rm**2)*(1./np.outer(amod**6,bmod**12) + 1./np.outer(amod**12,bmod**6))
    term12 =  np.exp(12.*logr_m + 84.*sig_rm**2)/(outer**6)
    terms = (term24 + term18 + term12)/outer**2
    rrt = np.einsum("ia, jb -> iajb" ,a,b)
    mat = np.einsum( "ij,iajb->ab",terms, rrt)
    
    mat = mat*144.*np.exp(2*np.log(eps_mod)+4*sig_eps**2)
    return mat
 
def integr_LJs_mat_meangen(a, b,  rm_mean , eps_mean,  sig_rm, sig_eps):   # LJ with integrated mean

    logr_m = np.log(rm_mean)
    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    outer = np.outer(amod , bmod)
    term24 = np.exp(24.*logr_m+ 276.*sig_rm**2)/outer**12
    term18 = - np.exp(18.*logr_m+ 153.*sig_rm**2)*(1./np.outer(amod**6,bmod**12) + 1./np.outer(amod**12,bmod**6))
    term12 =  np.exp(12.*logr_m + 66.*sig_rm**2)/outer**6
    terms = (term24 + term18 + term12)/outer**2
    rrt = np.einsum("ia, jb -> iajb" ,a,b)
    
    mat = np.einsum( "ij,iajb->ab",terms, rrt)
    mat = mat*144.*np.exp(2.*np.log(eps_mean)+sig_eps**2)
    if a is b:
        mat = (mat + mat.T)/2.
    return mat
    
def LJs_mat(a, b, rm, eps ):
    
    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    outer = np.outer(amod , bmod)
    term24 = rm**24/outer**12
    term18 = - rm**18 *(1./np.outer(amod**12,bmod**6) + 1./np.outer(amod**6,bmod**12))
    term12 =  rm**12 /outer**6
    terms = (term24 + term18 + term12)/outer**2
    rrt = np.einsum("ia, jb -> iajb" ,a,b)
    mat = np.einsum( "ij,iajb->ab",terms, rrt)
    mat = 144.*eps*eps*mat
    if (a == b).all():
        mat = (mat + mat.T)/2.
    return mat

def SW_mat(a, b, eq_ang, eps2 ):
    amod = np.sqrt((a**2).sum(1))
    bmod = np.sqrt((b**2).sum(1))
    outera = np.outer(amod, amod)
    outerb = np.outer(bmod, bmod)
    cosjk = np.einsum('jd, kd -> jk', a, a)/outera
    coslm = np.einsum('jd, kd -> jk', b, b)/outerb
    term1jklm = np.einsum('jk, lm -> jklm',cosjk,coslm)
    term2jklm =  -1./3. * (cosjk[:, :, None, None] + coslm[:, :, None, None].T) 
    term3 = np.array((1./3.)**2)
    a_sc = np.einsum('jdf, jf -> jd', a[:, :, None], 1./amod[:, None]**2)
    b_sc = np.einsum('jdf, jf -> jd', b[:, :, None], 1./bmod[:, None]**2)    
    Cjk1 = -np.einsum('jdkf, jkf -> jdk',(a[:, :, None] + a[:, :, None].T)[:, :, :, None], 1./outera[:, :, None]) 
    Cjk2 = np.einsum('jkf, jdkf -> jdk', cosjk[:, :, None] ,(a_sc[:, :, None] + a_sc[:, :, None].T)[:, :, :,None])
    Clm1 = -np.einsum('jdkf, jkf -> jdk',(b[:, :, None] + b[:, :, None].T)[:, :, :, None], 1./outerb[:, :, None]) 
    Clm2 = np.einsum('jkf, jdkf -> jdk', coslm[:, :, None] ,(b_sc[:, :, None] + b_sc[:, :, None].T)[:, :, :,None])
    Cjklmab = np.einsum('jak, lbm ->jklmab', Cjk1 + Cjk2, Clm1 + Clm2)
    result = np.einsum('jklmab , jklm -> ab', Cjklmab, term1jklm + term2jklm + term3[None, None, None, None])
    return result

def integr_harmonic_mat(a, b, sig_amp, sig_r0):
    mat = np.zeros((D, D))
    d_cut = 1.5
    for i in np.arange(len(a)):
        for j in np.arange(len(b)):
            ri, rj = a[i], b[j]
            di, dj = np.sqrt(ri.dot(ri)), np.sqrt(rj.dot(rj))
            if di <= d_cut and dj <= d_cut:
                term = (di*dj + np.exp(sig_r0**2) - di - dj)
                mat += term*np.outer(ri, rj)/(di*dj)
    mat = mat*np.exp(np.log(72*2) + sig_amp**2)
    return mat
            
        
class GaussianProcess3:
   
    def __init__(self, ker=['sim','LI_MAT'], fvecs =['cart','LJs'] ,theta0=[1e-1], nugget=1000. * MACHINE_EPSILON, sig = 0.5, optimizer="fmin_l_bfgs_b", \
                            bounds = (0.1, 10), calc_error = False, eval_grad = False):
        self.sig = sig
        self.theta0 = theta0
        self.ker = ker
        self.fvecs = fvecs
        self.nugget = nugget
        self.optimizer = optimizer
        self.bounds = bounds
        self.calc_error = calc_error
        self.eval_grad = eval_grad
        self.r_c = 5.#3.75
        
    def inv_ker(self, a, b):
        theta0 = self.stheta0
        sig = self.sig
        ker = self.sker
        eval_grad = self.eval_grad

        if ker == 'sim':
            simscaled = (sim(a, b, sig))
            result = simscaled#**theta0
            if eval_grad:
                grad = result * np.log(simscaled)
                return result, grad
            else:
                return (result)
        elif ker is 'SE':
            distsq =  - 2*sim(a, b, sig) + 2. #sim(a, a, sig) + sim(b, b, sig)
            result = np.exp(-distsq/(2.*theta0))
            if eval_grad:
                grad = result * distsq/(theta0**2)
                return result, grad
            else:
                return result
        elif ker is 'sim_cut':
            r_c = self.r_c
            result = (sim_cut(a, b, sig, r_c)/np.sqrt(sim_cut(a, a, sig, r_c)* sim_cut(b, b, sig, r_c)))**2
            return result 
        elif ker is 'inv_sim':
            result = (inv_sim(a, b, sig)/np.sqrt(inv_sim(a, a, sig)* inv_sim(b, b, sig)))**2
            
            return result
        elif ker is 'sim_sq':
            result = sim_sq(a, b, sig, 0)
            
            return result
        elif ker is 'inv_sim_sq':
            #print("a")
            result = (inv_sim_sq(a, b, sig)/np.sqrt(inv_sim_sq(a, a, sig)* inv_sim_sq(b, b, sig)))**2
            return result
        elif ker is "LI_MAT":
            v1s, v2s = self.v1s, self.v2s
            v1s_n, v2s_n = normalize(v1s, axis = 0), normalize(v2s, axis = 0)
            a_mat = v1s.dot(v1s_n.T)
            b_mat = v2s.dot(v2s_n.T)
            d_sqrd =1./self.LL *np.sum((a_mat - b_mat)**2)
            result = np.exp(-d_sqrd/(2*theta0))
            if eval_grad:
                grad = result * d_sqrd/(theta0**2)
                return result, grad

            else:
                return result
        elif ker == 'id':
            return 1.
        elif ker is "INERTIA_TEN":
            a_mat = a.dot(a.T)
            b_mat = b.dot(b.T)
            d_sqrd =1./self.LL *np.sum((a_mat - b_mat)**2)
            result = np.exp(-d_sqrd/(2*theta0))
            #print(result)
            return result
            
        else:
            print("Correlation model %s not understood" % ker)
            return None

    def feat_vecs(self, a, b):
        fvecs = self.sfvecs

        if fvecs == 'cart':

            self.LL = D
            f_vecsa, f_vecsb = np.zeros((self.LL, D)), np.zeros((self.LL, D))
            fvs = np.identity(D)
            for i in np.arange(D):
                f_vecsa[i], f_vecsb[i] = fvs[i], fvs[i]
            fvec_ten = np.einsum('ia, jb -> iajb',f_vecsa, f_vecsb)
        elif fvecs is 'gen':
            sig = self.sig
            theta0 = self.stheta0
            self.LL = 3
            f_vecsa, f_vecsb = np.zeros((self.LL, D)), np.zeros((self.LL, D))
            f_vecsa[0], f_vecsa[1], f_vecsa[2] = COM(a), m1_kSE(a, sig, theta0), m2_kSE(a, sig, theta0)
            f_vecsb[0], f_vecsb[1], f_vecsb[2] = COM(b), m1_kSE(b, sig, theta0), m2_kSE(b, sig, theta0)
            fvec_ten = np.einsum('ia, jb -> iajb',f_vecsa, f_vecsb)
        elif fvecs is 'LJ':
            self.LL = 1
            f_vecsa, f_vecsb = LJ_force(a, 2.8, 1.)[None, :], LJ_force(b, 2.8, 1.)[None, :]
            fvec_ten = np.einsum('ia, jb -> iajb',f_vecsa, f_vecsb)
        elif fvecs is 'LJs':
            self.LL = 25
            f_vecsa, f_vecsb = np.zeros((self.LL, D)), np.zeros((self.LL, D))
            
            epss, rms = np.linspace(1., 2., 5), np.linspace(2., 2.3, 5)
            rmeps = np.dstack(np.meshgrid(rms, epss)).reshape(-1, 2)
            f_vecsa, f_vecsb = LJ_force_params(a, rmeps[:, 0], rmeps[:, 1]),  LJ_force_params(b, rmeps[:, 0], rmeps[:, 1])
            self.v1s, self.v2s = f_vecsa, f_vecsb
            fvec_ten = np.einsum('ia, jb -> iajb',f_vecsa, f_vecsb)
        elif fvecs is 'LJ_int':
            self.LL = 1
            lj_mat = integr_LJs_mat_meangen(a, b, 2.8, 1., 0.02, 0.02)
            fvec_ten = lj_mat[None, :, None, :]
        elif fvecs == 'cov_mat':
            self.LL = 1
            sig = self.sig
            theta0 = self.theta0
            lj_mat = (cov_sim3D(a, b, sig))# - cov_sim3D(a, -  b, sig) )/2.#cov_sim3(a, b, sig)

            fvec_ten = lj_mat[None, :, None, :]
        elif fvecs is 'harm':           # note LJ equilibrium amplitude is 72
            self.LL = 9
            f_vecsa, f_vecsb = np.zeros((self.LL, D)), np.zeros((self.LL, D))
            idx = 0
            for amp in np.linspace(62, 82, 3):
                for rm in np.linspace(0.9, 1.1, 3):
                    f_vecsa[idx] = harmonic_force(a, rm, amp)
                    f_vecsb[idx] = harmonic_force(b, rm, amp)
                    idx += 1
            fvec_ten = np.einsum('ia, jb -> iajb',f_vecsa, f_vecsb)

        return fvec_ten
    
    def mat_kernel_func(self, a, b):   
        """
        Calculation of the matrix valued kernel function.
        
        Parameters
        ----------

        
        Returns
        -------
        K(a, b): matrix valued kernel function 
        """
        
        sig = self.sig
        theta0 = self.theta0
        ker = self.ker
        feat_vecs = self.feat_vecs
        inv_ker = self.inv_ker
        fvecs = self.fvecs
        K = np.zeros((D, D))
        eval_grad = self.eval_grad
        
        if eval_grad:
            K_g = np.zeros((len(theta0), D, D))
            for s in np.arange(len(ker)):
                self.sker = ker[s]
                self.sfvecs = fvecs[s]
                self.stheta0 = theta0[s]
                fvec_ten = feat_vecs(a, b)
                self.fvec_ten = fvec_ten
                k_inv, k_inv_g =  inv_ker(a, b)
                d_inv, d_inv_g = np.ones(self.LL) * k_inv, np.ones(self.LL) * k_inv_g
                K += np.einsum('iaib, i -> ab', fvec_ten, d_inv)
                K_g[s] += np.einsum('iaib, i -> ab', fvec_ten, d_inv_g)

            return K, K_g
        else:
            for s in np.arange(len(ker)):
                self.sker = ker[s]
                self.sfvecs = fvecs[s]
                self.stheta0 = theta0[s]
                fvec_ten = feat_vecs(a, b)
                self.fvec_ten = fvec_ten
                k_inv = np.ones(self.LL) * inv_ker(a, b)
                K += np.einsum('iaib, i -> ab', fvec_ten, k_inv)
            
            return K
        
    def calc_kernel_matrix(self, X, sig, theta0):                                                    
        """
        Calculation of the Gram Matrix.
        
        Parameters
        ----------
        X : array with shape (n_samples, n_features)
        
        Returns
        -------
        K: the Gram Matrix (covariance matrix) of the data
        """
        self.theta0 = theta0
        ker = self.ker
        mat_kernel_func = self.mat_kernel_func
        Ntrain = self.Ntrain
        diag = np.identity(Ntrain*D)*self.nugget
        off_diag = np.zeros((Ntrain*D, Ntrain*D))
        eval_grad = self.eval_grad
        
        if eval_grad:
            g_diag = np.zeros((len(theta0), Ntrain*D, Ntrain*D))
            g_off_diag = np.zeros((len(theta0), Ntrain*D, Ntrain*D))
            for i in np.arange(Ntrain):
                k, kg = mat_kernel_func(X[i], X[i])
                diag[D*i:D*i+D, D*i:D*i+D]  += k
                g_diag[:, D*i:D*i+D, D*i:D*i+D]  += kg + self.nugget
            for i in np.arange(Ntrain):
                for j in np.arange(i): 
                    off_diag[D*i:D*i+D, D*j:D*j+D], g_off_diag[:, D*i:D*i+D, D*j:D*j+D] = mat_kernel_func(X[i], X[j])
            K = diag + off_diag + off_diag.T
            K_g = g_diag +  g_off_diag + np.transpose(g_off_diag, (0, 2, 1))
            self.K = K
            #print("Is K symmetric? ", (K == K.T).all())
            #print("Is K positive definite?  ", (np.linalg.eigvalsh(K)> 0).all())
            return K, K_g
                    
        else:
            for i in np.arange(Ntrain):
                diag[D*i:D*i+D, D*i:D*i+D] +=  mat_kernel_func(X[i], X[i])
                
            for i in np.arange(Ntrain):
                for j in np.arange(i): 
                    off_diag[D*i:D*i+D, D*j:D*j+D] = mat_kernel_func(X[i], X[j])
            K = diag + off_diag + off_diag.T
            np.set_printoptions(precision = 3)
            self.K = K
            print("Is K symmetric? ", (K == K.T).all())
            eigs = np.linalg.eigvalsh(K)
            print("Is K positive definite?  ", (eigs> 0).all())

            return K

    def log_marginal_likelihood(self, sig, theta0):
            
        Xtrain = self.Xtrain
        Xtrain = self.Xtrain
        eval_grad = self.eval_grad
        
        if eval_grad:
            K, K_g = self.calc_kernel_matrix(Xtrain, sig, theta0)
        else:
            K = self.calc_kernel_matrix(Xtrain, sig, theta0)
            
        L = cholesky(K, lower = True)
        alpha = cho_solve((L, True), self.ytrain)
        
        log_likelihood = -0.5 * np.dot(self.ytrain.T,alpha)
        log_likelihood -= np.log(np.diag(L)).sum()
        log_likelihood -= K.shape[0] / 2. * np.log(2. * np.pi)
        
        if eval_grad:
            tmp = np.outer(alpha, alpha)
            tmp -= cho_solve((L, True), np.eye(K.shape[0]))
            log_likelihood_g = 0.5 * np.einsum('lj , slj -> s', tmp, K_g )
            return log_likelihood, log_likelihood_g
        else:
            return log_likelihood

    def constrained_optimization(self, obj_func, initial_theta, bounds):
        approx = not self.eval_grad
        if self.optimizer == "fmin_l_bfgs_b":
            theta0_opt, func_min, convergence_dict = fmin_l_bfgs_b(obj_func, initial_theta, bounds=bounds, approx_grad=approx, iprint = 1)
            print(theta0_opt)
            print("fmin_l_bfgs_b theta0_opt and func_min are ",theta0_opt, func_min,convergence_dict )
        elif self.optimizer == "Nelder-Mead":
            optimum = minimize(fun=obj_func, x0=initial_theta, args=(), method= "Nelder-Mead", bounds=bounds,  options={'disp': True } )
            print(optimum)
            theta0_opt,   func_min = optimum.x, optimum.fun
            print("Nelder-Mead theta0_opt and func_min are ",theta0_opt, func_min,optimum.message )
        return theta0_opt, func_min

    def fit(self, X=None, y_ten=None):
        """
        The Gaussian Process model fitting method.

        Parameters
        ----------
        X : array with shape (n_samples, n_features)

        y : array with shape (n_samples)
        
        Returns
        -------
        gp : self
            A fitted Gaussian Process model object awaiting data to perform
            predictions.
        """
        self.Xtrain = X
        self.Ntrain = len(X)
        y = np.resize(y_ten,(len(y_ten)*D, 1))
        self.ytrain = y
        eval_grad = self.eval_grad
        
        if self.optimizer is not None:

            def obj_func(theta0):
                    if eval_grad:
                        lml, lml_grad = self.log_marginal_likelihood(self.sig, theta0)
                        print("D: ",theta0, -lml, -lml_grad)
                        return -lml, -lml_grad
                    else:
                        lml = self.log_marginal_likelihood(self.sig, theta0)
                        print("D: ",theta0, -lml)
                        return -lml
                    
            # First optimize starting from theta specified in kernel
            initial_theta = self.theta0
            theta_opt, func_min  = self.constrained_optimization(obj_func, initial_theta, self.bounds)
            self.theta0 = theta_opt
            self.log_marginal_likelihood_value = -func_min

        sig = self.sig
        theta0 = self.theta0
        calc_kernel_matrix = self.calc_kernel_matrix
        self.eval_grad = False
        K = calc_kernel_matrix(X, sig, theta0)

        # invert covariance matrix
        try:
            inv = LA.pinv2(K)
        except LA.LinAlgError as err:
            print("pinv2 failed: %s. Switching to pinvh" % err)
            try:
                inv = LA.pinvh(K)
            except LA.LinAlgError as err:
                print("pinvh failed: %s. Switching to pinv2" % err)
                inv = None

        # alpha is the vector of regression coefficients of GaussianProcess
        inv_ten = np.reshape(inv, (self.Ntrain, D, self.Ntrain,  D))
        self.inv_ten = inv_ten
        self.K = K

        self.alpha_ten = np.einsum('ndND, ND -> nd ',inv_ten,y_ten)
        
        # second method, cholesky decomposition
        """
        self.L = cholesky(K, lower = True)
        self.alpha2 = cho_solve((self.L, True), self.ytrain)
        idx = np.argmax(np.abs(self.alpha-self.alpha2))
        
        print(self.alpha[idx],self.alpha2[idx], np.abs(self.alpha-self.alpha2)[idx],np.max(np.abs(self.alpha-self.alpha2)),np.mean(np.abs(self.alpha)),np.mean(np.abs(self.alpha2)),np.mean(np.abs(self.alpha-self.alpha2)), )
        """

    def predict(self, X):
        """
        This function evaluates the Gaussian Process model at X.
    
        Returns
        -------
        y : array_like
        """

        Xtrain = self.Xtrain
        Ntrain = self.Ntrain
        self.X = X
        calc_error = self.calc_error
        K2 = np.zeros((len(X)*D, Ntrain*D))
        K2_ten = np.zeros((len(X), Ntrain, D, D))
        ker = self.ker
        theta0 = self.theta0
        sig = self.sig
        nugget = self.nugget
        mat_kernel_func = self.mat_kernel_func
        inv_ker = self.inv_ker
            
        K2_ten = np.array([mat_kernel_func(X[i], Xtrain[j])for i in np.arange(len(X)) for j in np.arange(Ntrain)])
        K2_ten.resize((len(X), Ntrain, D, D))

        pred =  np.einsum(' nNdp, Np -> nd' , K2_ten, self.alpha_ten)

        if calc_error == True:
                var = np.zeros((len(X), 3))

                for i in np.arange(len(X)):
                        kv = K2_ten[i]
                        var[i] = np.diag(mat_kernel_func(X[i], X[i]))
                        var[i] += nugget
                        var[i] = np.diag(np.einsum('Uab, UbDc, Ddc -> ad', kv, self.inv_ten, kv)) 

                return pred, var
        else:
                
                return pred
	

